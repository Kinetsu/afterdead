#pragma once

#include "Color.h"

#include <unordered_map>

namespace HL {
	class Draw {
	private:
		const DWORD CustomFVF = D3DFVF_XYZRHW | D3DFVF_DIFFUSE;
		struct CustomVertex {
			float x, y, z, rhw;
			DWORD color;
		};

		char Buffer[1024];
		LPD3DXLINE Line;
		std::unordered_map<char*, LPD3DXFONT> Fonts;
		LPD3DXFONT CurrentFont;

	public:
		LPDIRECT3DDEVICE9 Device;

		Draw(LPDIRECT3DDEVICE9 device);
		
		static DWORD ToAlphaColor(DWORD color, int alpha);
		static DWORD GetAlpha(DWORD color);

		void AddFont(char* name, INT height, UINT weight, char* faceName);
		void SetFont(char* name);
		RECT GetTextRect(const char* text);
		void DrawLine(D3DXVECTOR2 begin, D3DXVECTOR2 end, D3DCOLOR color);
		void DrawLines(D3DXVECTOR2* lines, int lineCount, D3DCOLOR color);
		void DrawString(int x, int y, DWORD color, const char* text, bool center = false);
		void DrawString(int x, int y, DWORD color, bool center, const char* text, ...);
		void DrawStringWithOutline(int x, int y, DWORD color, bool center, const char* text, ...);
		void DrawRect(int x, int y, int width, int height, D3DCOLOR color, bool center = false);
		void DrawFilledRect(int x, int y, int width, int height, D3DCOLOR color, bool center = false);
		void Release();
	};
}