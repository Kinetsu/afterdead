#pragma once

#include <Windows.h>
#include <fstream>

namespace HL {
	class Log {
	private:
		std::ofstream FileStream;
		char Buffer[1024];
		bool LogToFile;
		bool LogToConsole;

		void WriteToFile(char* text);
		void WriteToConsole(char* text);

	public:
		Log(bool logToConsole = true, bool logToFile = true);

		void Write(const char* format, ...);
		void Release();
	};
}