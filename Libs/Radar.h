#pragma once
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "Draw.lib")

#include <d3d9.h>
#include <d3dx9.h>
#include <Draw.h>

namespace HL {
	class Radar {
	private:
		Draw* Drawer = NULL;
		
	public:
		int X = 200;
		int Y = 200;
		int RadarSize = 200;
		int Range = 300;
		int DotSize = 4;

		Radar(Draw* drawer);

		void DrawRadarStructure();
		void DrawRadarPoint(D3DXVECTOR3 localPlayerPosition, D3DXVECTOR3 targetPosition, float localPlayerYaw, DWORD color);
	};
}