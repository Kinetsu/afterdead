#pragma once
#pragma comment(lib, "Draw.lib")

#include <string>
#include <Draw.h>

namespace HL {
	enum ItemType {
		Label,
		CheckBox
	};

	enum ItemValueType {
		NoValue,
		BoolValue,
		IntValue,
		FloatValue,
		StringValue
	};

	struct ItemValue {
		bool BoolValue;
		int IntValue;
		float FloatValue;
		std::string StringValue;
	};

	struct MenuItem {
		std::string Name;
		ItemType Type;
		DWORD Color;
		DWORD ColorHover;
		RECT Margins;
		ItemValueType ValueType;
		ItemValue Value;
	};

	class Menu {
	private:
		static const int MAX_MENU_ITEMS = 25;
		const int PADDING = 5;
		int CurrentDrawX = 0;
		int CurrentDrawY = 0;

		POINT MousePosition;
		POINT MouseLastClick;
		Draw* Drawer = NULL;
		bool Display = false;
		bool IsHoldingKey = false;
		bool IsDragging = false;
		int MenuItemsCount = 0;
		DWORD BackgroundColor = 0;
		DWORD BorderColor = 0;

		void RenderItems();
		void RenderCursor();
		void DragMenu();
		bool IsCursorOverMenu();
		bool IsCursorOverItem(MenuItem item);

	public:
		int X = 20;
		int Y = 20;
		int Width = 200;
		int Height = 500;
		bool ShowAways = false;
		bool Cursor = true;
		bool LabelValuesAtRight = false;
		bool UseDifferentColorForValues = false;
		DWORD ValueColor;
		MenuItem MenuItems[Menu::MAX_MENU_ITEMS];

		Menu(Draw* drawer, DWORD backgroundColor, DWORD borderColor);
		void AddMenuItem(std::string name, ItemType type, DWORD color);
		void AddMenuItem(std::string name, ItemType type, DWORD color, DWORD colorHover);
		void AddMenuItem(std::string name, ItemType type, DWORD color, DWORD colorHover, ItemValueType valueType, ItemValue value);
		void Render();
		bool GetBoolValueByName(std::string name);
		int GetIntValueByName(std::string name);
		float GetFloatValueByName(std::string name);
		std::string GetStringValueByName(std::string name);
		void SetBoolValueByName(std::string name, bool value);
		void SetIntValueByName(std::string name, int value);
		void SetFloatValueByName(std::string name, float value);
		void SetStringValueByName(std::string name, std::string value);
	};
}