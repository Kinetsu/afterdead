#pragma once

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include <d3d9.h>
#include <d3dx9.h>

namespace HL {
	class Color {
	public:
		static const DWORD BLACK = D3DCOLOR_ARGB(255, 0, 0, 0);
		static const DWORD WHITE = D3DCOLOR_ARGB(255, 255, 255, 255);
		static const DWORD RED = D3DCOLOR_ARGB(255, 255, 0, 0);
		static const DWORD DARK_RED = D3DCOLOR_ARGB(255, 175, 0, 0);
		static const DWORD ORANGE = D3DCOLOR_ARGB(255, 255, 150, 0);
		static const DWORD DARK_ORANGE = D3DCOLOR_ARGB(255, 200, 100, 0);
		static const DWORD YELLOW = D3DCOLOR_ARGB(255, 255, 255, 0);
		static const DWORD DARK_YELLOW = D3DCOLOR_ARGB(255, 150, 150, 0);
		static const DWORD LIME = D3DCOLOR_ARGB(255, 150, 255, 0);
		static const DWORD DARK_LIME = D3DCOLOR_ARGB(255, 100, 200, 0);
		static const DWORD GREEN = D3DCOLOR_ARGB(255, 0, 255, 0);
		static const DWORD DARK_GREEN = D3DCOLOR_ARGB(255, 0, 175, 0);
		static const DWORD CYAN = D3DCOLOR_ARGB(255, 0, 255, 255);
		static const DWORD DARK_CYAN = D3DCOLOR_ARGB(255, 0, 150, 150);
		static const DWORD BLUE = D3DCOLOR_ARGB(255, 0, 0, 255);
		static const DWORD DARK_BLUE = D3DCOLOR_ARGB(255, 0, 0, 175);
		static const DWORD PURPLE = D3DCOLOR_ARGB(255, 150, 0, 255);
		static const DWORD DARK_PURPLE = D3DCOLOR_ARGB(255, 100, 0, 200);
		static const DWORD PINK = D3DCOLOR_ARGB(255, 255, 0, 255);
		static const DWORD DARK_PINK = D3DCOLOR_ARGB(255, 150, 0, 150);
		static const DWORD GRAY = D3DCOLOR_ARGB(255, 150, 150, 150);
		static const DWORD DARK_GRAY = D3DCOLOR_ARGB(255, 100, 100, 100);
	};
}