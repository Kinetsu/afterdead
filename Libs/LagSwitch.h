#pragma once

namespace HL {
	class LagSwitch {
	private:
		char* LagSwitchRuleName = "";
		char* TargetRuleName = "";
		char* TargetExePath = "";

		void Init();

	public:
		bool IsOn = false;

		LagSwitch(char* lagSwitchRuleName, char* targetRuleName, char* targetExePath);
		
		void Switch();
		void Release();
	};
}