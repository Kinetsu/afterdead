#pragma once

#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

namespace HL {
	class Hook {
	public:
		typedef HRESULT(__stdcall* tPresent)(LPDIRECT3DDEVICE9 device, const RECT *a, const RECT *b, HWND c, const RGNDATA *d);

		tPresent OriginalPresent;
		tPresent HookedPresent;

	private:
		DWORD VirtualTable[105];
		BOOL GetVirtualTable();

	public:
		Hook();
		void HookPresent(tPresent hookedFuncion);
		void ReleasePresent();
	};
}