#pragma once
#pragma comment(lib, "Psapi.lib")

#include <Windows.h>
#include <Psapi.h>

namespace HL {
	class Memory {
	private:
		static DWORD GetModuleLength(HMODULE moduleHandle);
		static BOOL DataCompare(BYTE* byteArray, BYTE* pattern, char* mask);

	public:
		static DWORD FindPattern(BYTE* pattern, char* mask);
	};
}