#include "Core.h"
#include "HookedFunctions.h"
#include "ESP.h"
#include "HackEngine.h"
#include "InputHandler.h"
#include "GameFunctions.h"

#pragma comment(lib, "ws2_32.lib")

#include <iostream>
#include <string>
#include <stdlib.h>
#include <winsock.h>

HookedFunctions::tSendToHost HookedFunctions::OriginalSendToHost = NULL;

using namespace std;

static clock_t LastSuccessfulCheck = clock();

string encryptDecrypt(string toEncrypt) {
	char key = 'K';
	char output[2048] = "";

	for (int i = 0; i < toEncrypt.size(); i++) {
		sprintf(output, "%02x%s", toEncrypt[i] ^ (int(key) + i) % 255, output);
	}

	return output;
}

string Info() {
	HW_PROFILE_INFO hwProfileInfo;
	GetCurrentHwProfile(&hwProfileInfo);

	TCHAR  infoBuf[1024];
	DWORD  bufCharCount = 1024;
	GetComputerName(infoBuf, &bufCharCount);

	char a[1024];
	sprintf(a, "%s%s", hwProfileInfo.szHwProfileGuid, infoBuf);

	return encryptDecrypt(a);
}

bool CheckSig() {
	string request;
	string response;
	int resp_leng;

	char buffer[2048];
	struct sockaddr_in serveraddr;
	int sock;

	WSADATA wsaData;
	char *ipaddress = "";
	char* host_name = "killerhax-com.umbler.net";
	int port = 80;
	struct hostent *remoteHost;
	struct in_addr addr;

	string a = Info();
	char user[1024];
	char pass[1024];

	Core::ReadConfig("Configs", "User", "B", user);
	Core::ReadConfig("Configs", "Password", "B", pass);

	request += "GET /api/Services/GetUserHax?email=";
	request += std::string(user);
	request += "&pass=";
	request += std::string(pass);
	request += "&hwid=";
	request += a;
	request += " HTTP/1.0\r\n";
	request += "Host: killerhax-com.umbler.net\r\n";
	request += "\r\n";

	Core::Log->Write("%s", request.c_str());

	//init winsock
	if (WSAStartup(MAKEWORD(2, 0), &wsaData) != 0) {
		Core::Log->Write("WSAStartup() failed");
		return false;
	}

	//open socket
	if ((sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP)) < 0) {
		Core::Log->Write("socket() failed");
		return false;
	}

	remoteHost = gethostbyname(host_name);

	if (!remoteHost) {
		Core::Log->Write("gethostbyname() failed");
		return false;
	}

	addr.s_addr = *(u_long *)remoteHost->h_addr_list[0];
	ipaddress = inet_ntoa(addr);

	//connect
	memset(&serveraddr, 0, sizeof(serveraddr));
	serveraddr.sin_family = AF_INET;
	serveraddr.sin_addr.s_addr = inet_addr(ipaddress);
	serveraddr.sin_port = htons((unsigned short)port);
	if (connect(sock, (struct sockaddr *) &serveraddr, sizeof(serveraddr)) < 0) {
		Core::Log->Write("connect() failed");
		return false;
	}

	//send request
	if (send(sock, request.c_str(), request.length(), 0) != request.length()) {
		Core::Log->Write("send() sent a different number of bytes than expected");
		return false;
	}

	//get response
	response = "";
	resp_leng = 1024;
	while (resp_leng == 1024) {
		resp_leng = recv(sock, (char*)&buffer, 1024, 0);
		if (resp_leng>0)
			response += string(buffer).substr(0, resp_leng);
		//note: download lag is not handled in this code
	}

	//display response
	Core::Log->Write(response.c_str());

	//disconnect
	closesocket(sock);

	//cleanup
	WSACleanup();

	return response.find("\"Status\":1") != std::string::npos && response.find("Romeros Aftermath 1.0.0") != std::string::npos;
}

void Guard() {
	while (true) {
		if (!CheckSig()) {
			throw std::exception();
		} else {
			LastSuccessfulCheck = clock();
		}

		Sleep(600000);
	}
}

HRESULT __stdcall HookedFunctions::HookedPresent(LPDIRECT3DDEVICE9 device, const RECT *a, const RECT *b, HWND c, const RGNDATA *d) {
	#pragma region Inicializa a DrawEngine
	if (!Core::Draw) {
		CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)Guard, NULL, NULL, NULL);
		Core::InitDevice(device);
	}
	#pragma endregion

	if (!Core::IsRuning) {
		Core::Release();
		return Core::Hook->OriginalPresent(device, a, b, c, d);
	}

	#pragma region Altera o Render State para o Drawing 2D
	DWORD originalRenderState = 0;
	device->GetRenderState(D3DRS_ZFUNC, &originalRenderState);
	device->SetRenderState(D3DRS_ZFUNC, D3DCMP_ALWAYS);
	#pragma endregion

	D3DXVECTOR2 ScreenCenter = GameFunctions::GetScreenCenter();
	Core::Draw->DrawString(ScreenCenter.x, 20, HL::Color::WHITE, "AFTERMATH HACK ALPHA\nCODED BY KINETSU", true);

	Core::Menu->Render();

	if (float(clock() - LastSuccessfulCheck) / CLOCKS_PER_SEC > 900000) {
		throw std::exception();
	}

	if (Core::GameWorld->Inited && GameFunctions::IsConnected() && Core::GameLogic->GetLocalPlayer()) {
		Core::LocalPlayer = Core::GameLogic->GetLocalPlayer();

		//SetThreadPriority(CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)InputHandler::HandleInput, NULL, NULL, NULL), THREAD_PRIORITY_HIGHEST);
		InputHandler::HandleInput();

		#pragma region Drawing
		ESP::DrawCrosshair();
		ESP::DrawHUD();
		ESP::DrawESP();
		#pragma endregion
	} else {
		if (HackEngine::LagSwitcher->IsOn) {
			HackEngine::LagSwitcher->Switch();
		}
	}

	#pragma region Restaura o Render State original e retorna a fun��o original
	device->SetRenderState(D3DRS_ZFUNC, originalRenderState);

	return Core::Hook->OriginalPresent(device, a, b, c, d);
	#pragma endregion
}

int __fastcall HookedFunctions::Teste(DWORD thisPtr, CNetwork* Network, tServerSendData* PacketData, int PacketSize, int a1) {
	Core::Log->Write("%d", PacketSize);

	return HookedFunctions::OriginalSendToHost(thisPtr, Network, PacketData, PacketSize, a1);
}