#pragma once

#include "Skeleton.h"

namespace GameClasses {
	class Zombie {
	public:
		char _0x0000[36];		//0x0000
		D3DXVECTOR3 Position;	//0x0024
		char _0x0030[424];		//0x0030
		D3DXMATRIX BoneCoeff;	//0x01D8
		char _0x0218[40];		//0x0218
		int ObjectType;			//0x0240
		char _0x0244[452];		//0x0244
		char* Name;				//0x0408
		char _0x040C[28];		//0x040C
		DWORD ItemId;			//0x0428
		char _0x042C[36];		//0x042C
		BoneInfo* BoneInfo;		//0x0450
		char _0x0454[8568];		//0x0454
		int ZombieState;		//0x25CC

		D3DXVECTOR3 GetBonePosition(int boneID) {
			if (!BoneInfo)
				return D3DXVECTOR3(0, 0, 0);

			D3DXMATRIX rotationMatrix;
			D3DXMATRIX boneMatrix;
			D3DXMATRIX resultMatrix;
			D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0);

			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0.0, D3DXToRadian(90), 0.0);
			boneMatrix = this->BoneInfo->Bones[boneID].BoneMatrix;
			D3DXMatrixMultiply(&boneMatrix, &boneMatrix, &BoneCoeff);
			D3DXMatrixMultiply(&resultMatrix, &rotationMatrix, &boneMatrix);

			position.x = resultMatrix._41;
			position.y = resultMatrix._42;
			position.z = resultMatrix._43;

			return position;
		}
	};
}