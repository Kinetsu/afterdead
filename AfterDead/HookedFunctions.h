#pragma once
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include <d3d9.h>
#include <d3dx9.h>
#include <Windows.h>

class HookedFunctions {
public:
	struct tServerSendData {
		BYTE FirstFreeEventID;
		BYTE PacketType;   // this BYTE
		WORD  NetworkID;   //0001
						   //unsigned char data[1024]; //0003
	};

	class vSendToHost {
		BYTE FirstFreeEventID;
		int PacketType;
		WORD  NetworkID;
	};

	class CNetwork {
	public:
		DWORD* impl; // 0000
		int dumpStats_; // 0004
		int lastPing_; // 0008
	};

	typedef int(__fastcall* tSendToHost)(DWORD thisPtr, CNetwork* Network, tServerSendData* PacketData, int PacketSize, int a1);

	static tSendToHost OriginalSendToHost;
	static int __fastcall HookedFunctions::Teste(DWORD thisPtr, CNetwork* Network, tServerSendData* PacketData, int PacketSize, int a1);

	static HRESULT __stdcall HookedPresent(LPDIRECT3DDEVICE9 device, const RECT *a, const RECT *b, HWND c, const RGNDATA *d);
};