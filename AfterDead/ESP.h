#pragma once

#include "GameObject.h"
#include "Player.h"
#include "Zombie.h"

class ESP {
private:
	static int AggroZombieCount;
	static int NearPlayersCountAux;
	static int NearPlayersCount;
	static int ClosestPlayerDistanceToAim;
	static GameClasses::Player* Target;

	static void DrawPlayer(GameClasses::Player* player);
	static void DrawZombie(GameClasses::Zombie* zombie);
	static void DrawObject(GameClasses::GameObject* object);
	static void DrawObjectDebug(GameClasses::GameObject* object);
	static void DrawPlayerSkeleton(GameClasses::Player* player, DWORD color);
	static void DrawZombieSkeleton(GameClasses::Zombie* zombie, DWORD color);
	static void DrawHealthBar(int x, int y, float health, int alpha = 255);

public:
	static void DrawCrosshair();
	static void DrawHUD();
	static void DrawESP();
};