#include "ESP.h"
#include "Core.h"
#include "Offsets.h"
#include "GameObjectEnums.h"
#include "GameFunctions.h"
#include "HackConfig.h"
#include "HackEngine.h"
#include "KeyCode.h"
#include "MenuUtil.h"

#pragma region Private Properties
int ESP::AggroZombieCount = 0;
int ESP::NearPlayersCountAux = 0;
int ESP::NearPlayersCount = 0;
int ESP::ClosestPlayerDistanceToAim = 0;
GameClasses::Player* ESP::Target = NULL;
#pragma endregion

#pragma region Public
void ESP::DrawCrosshair() {
	D3DXVECTOR2 screenCenter = GameFunctions::GetScreenCenter();
	int size = 5;
	int centralCleanSpace = 3;

	Core::Draw->DrawLine(D3DXVECTOR2(screenCenter.x - size - centralCleanSpace, screenCenter.y), D3DXVECTOR2(screenCenter.x - centralCleanSpace, screenCenter.y), HL::Color::RED);
	Core::Draw->DrawLine(D3DXVECTOR2(screenCenter.x + centralCleanSpace, screenCenter.y), D3DXVECTOR2(screenCenter.x + size + centralCleanSpace, screenCenter.y), HL::Color::RED);
	Core::Draw->DrawLine(D3DXVECTOR2(screenCenter.x, screenCenter.y - size - centralCleanSpace), D3DXVECTOR2(screenCenter.x, screenCenter.y - centralCleanSpace), HL::Color::RED);
	Core::Draw->DrawLine(D3DXVECTOR2(screenCenter.x, screenCenter.y + centralCleanSpace), D3DXVECTOR2(screenCenter.x, screenCenter.y + size + centralCleanSpace), HL::Color::RED);
}

void ESP::DrawHUD() {
	char text[60];

	Core::HUD->MenuItems[HUD::Server].Value.StringValue = Core::GameLogic->GetServerName();

	sprintf(text, "%dms %p", Core::GameLogic->Ping, Core::LocalPlayer);
	Core::HUD->MenuItems[HUD::Ping].Value.StringValue = text;

	sprintf(text, "%d", Core::LocalPlayer->TotalKills);
	Core::HUD->MenuItems[HUD::Kills].Value.StringValue = text;

	sprintf(text, "%d", Core::LocalPlayer->TotalDeaths);
	Core::HUD->MenuItems[HUD::Deaths].Value.StringValue = text;

	sprintf(text, "%d", Core::LocalPlayer->Reputation);
	Core::HUD->MenuItems[HUD::Reputation].Value.StringValue = text;

	sprintf(text, "%.0f%%", Core::LocalPlayer->GetHeath());
	Core::HUD->MenuItems[HUD::Health].Value.StringValue = text;

	GameClasses::BaseItemConfig* backpack = GameFunctions::GetItemConfig(Core::LocalPlayer->BackpackId);
	sprintf(text, "%s", backpack ? backpack->StoreName : "Empty");
	Core::HUD->MenuItems[HUD::Backpack].Value.StringValue = text;

	if (Core::LocalPlayer->WeaponPrimary && Core::LocalPlayer->WeaponPrimary->Armory) {
		sprintf(text, "%s", Core::LocalPlayer->WeaponPrimary->Armory->Name);
	} else {
		sprintf(text, "Empty");
	}
	Core::HUD->MenuItems[HUD::Weapon1].Value.StringValue = text;

	if (Core::LocalPlayer->WeaponSecondary && Core::LocalPlayer->WeaponSecondary->Armory) {
		sprintf(text, "%s", Core::LocalPlayer->WeaponSecondary->Armory->Name);
	} else {
		sprintf(text, "Empty");
	}
	Core::HUD->MenuItems[HUD::Weapon2].Value.StringValue = text;

	sprintf(text, "%d", HackEngine::NoClipSpeed);
	Core::HUD->MenuItems[HUD::NoclipSpeed].Value.StringValue = text;

	sprintf(text, "%d", HackEngine::SpeedHackSpeed);
	Core::HUD->MenuItems[HUD::SpeedhackSpeed].Value.StringValue = text;

	sprintf(text, "%s", Core::Menu->MenuItems[Menu::GhostJump].Value.BoolValue ? "ON" : "OFF");
	Core::HUD->MenuItems[HUD::HudGhostJump].Value.StringValue = text;

	sprintf(text, "%s", Core::Menu->MenuItems[Menu::PlayerAutoMagnet].Value.BoolValue ? "ON" : "OFF");
	Core::HUD->MenuItems[HUD::AutoMagnet].Value.StringValue = text;

	sprintf(text, "%s", Core::Menu->MenuItems[Menu::AutoHeal].Value.BoolValue ? "ON" : "OFF");
	Core::HUD->MenuItems[HUD::HudAutoHeal].Value.StringValue = text;

	char* compass;
	if (Core::LocalPlayer->Yaw > 22.5 && Core::LocalPlayer->Yaw < 67.5) {
		compass = "NW";
	} else if (Core::LocalPlayer->Yaw > 67.5 && Core::LocalPlayer->Yaw < 112.5) {
		compass = "W";
	} else if (Core::LocalPlayer->Yaw > 112.5 && Core::LocalPlayer->Yaw < 157.5) {
		compass = "SW";
	} else if (Core::LocalPlayer->Yaw > 157.5 && Core::LocalPlayer->Yaw < 202.5) {
		compass = "S";
	} else if (Core::LocalPlayer->Yaw > 202.5 && Core::LocalPlayer->Yaw < 247.5) {
		compass = "SE";
	} else if (Core::LocalPlayer->Yaw > 247.5 && Core::LocalPlayer->Yaw < 292.5) {
		compass = "E";
	} else if (Core::LocalPlayer->Yaw > 292.5 && Core::LocalPlayer->Yaw < 337.5) {
		compass = "NE";
	} else {
		compass = "N";
	}
	Core::HUD->MenuItems[HUD::Compass].Value.StringValue = text;

	sprintf(text, "%s", HackEngine::TeleportLocations[HackEngine::TeleportLocationIndex].Name);
	Core::HUD->MenuItems[HUD::Teleport].Value.StringValue = text;

	if (GameFunctions::IsValidPlayer(Core::Target)) {
		char playerName[60];
		Core::Target->GetName(playerName);
		
		if (HackEngine::IsTargetLocked) {
			sprintf(text, "%s[%s]", Core::Target->AggressorFlag == 1 ? "!" : "", playerName);
		} else {
			sprintf(text, "%s%s", Core::Target->AggressorFlag == 1 ? "!" : "", playerName);
		}
	} else {
		sprintf(text, "No Target");
	}
	Core::HUD->MenuItems[HUD::AimbotTarget].Value.StringValue = text;

	Core::HUD->Render();
}

void ESP::DrawESP() {
	if (HackEngine::IsShowingTeleportScreen) {
		D3DXVECTOR2 screenCenter = GameFunctions::GetScreenCenter();
		Core::Draw->DrawFilledRect(screenCenter.x, screenCenter.y, screenCenter.x*2, screenCenter.y*2, HL::Color::BLACK, true);
		Core::Draw->SetFont("Tahoma Medium");
		Core::Draw->DrawString(screenCenter.x, screenCenter.y, HL::Color::RED, "TELEPORT IN PROGESS, PLEASE WAIT.", true);
		Core::Draw->SetFont("Tahoma Small");
		return;
	}

	ESP::AggroZombieCount = 0;
	ESP::NearPlayersCountAux = 0;
	ESP::ClosestPlayerDistanceToAim = -1;

	Core::Radar->DrawRadarStructure();

	if (!GameFunctions::IsValidPlayer(Core::Target) || (!Core::Menu->MenuItems[Menu::KeepTargetLockedOnDeath].Value.BoolValue && Core::Target->Dead == 1)) {
		Core::Target = NULL;
		ESP::Target = NULL;
		HackEngine::IsTargetLocked = false;
	}

	GameClasses::GameObject* object = Core::GameWorld->FirstObject;

	do {
		object = object->NextObject;

		if (!object || object == (GameClasses::GameObject*)Core::LocalPlayer) {
			continue;
		}

		switch (object->ObjectType) {
			case GameObjectEnums::ObjectType::Human:
				ESP::DrawPlayer((GameClasses::Player*)object);
				break;
			case GameObjectEnums::ObjectType::DroppedItem:
			case GameObjectEnums::ObjectType::Trap:
				ESP::DrawObject(object);
				break;
			case GameObjectEnums::ObjectType::Zombie:
				ESP::DrawZombie((GameClasses::Zombie*)object);
				break;
		}
	} while (object->NextObject);

	char alerts[200] = "";

	if (ESP::NearPlayersCountAux > 0) {
		sprintf(alerts, "%d Near Player(s)", ESP::NearPlayersCountAux);
	}

	if (ESP::AggroZombieCount > 0) {
		sprintf(alerts, "%s\nZombies Aggro", alerts);
	}

	if (Core::LocalPlayer->AggressorFlag == 1) {
		sprintf(alerts, "%s\nYou are a PVP aggressor for %.0f seconds", alerts, Core::LocalPlayer->AggressorTimer - Core::LocalPlayer->GameCounter);
	}

	if (HackEngine::LagSwitcher->IsOn && HackEngine::GetGhostModeElapsedTime() >= 9) {
		HackEngine::LagSwitcher->Switch();
	}

	if (HackEngine::LagSwitcher->IsOn) {
		sprintf(alerts, "%s\nGHOST MODE ON\n%.1f seconds left", alerts, (9 - HackEngine::GetGhostModeElapsedTime()));
	}

	Core::Draw->SetFont("Tahoma Medium");
	Core::Draw->DrawString(GameFunctions::GetScreenCenter().x, 150, HL::Color::RED, alerts, true);
	Core::Draw->SetFont("Tahoma Small");

	Core::Target = ESP::Target;
	ESP::NearPlayersCount = ESP::NearPlayersCountAux;
}
#pragma endregion

#pragma region Private
void ESP::DrawPlayer(GameClasses::Player* player) {
	DWORD textColor;
	D3DXVECTOR3 screenPosition;
	D3DXVECTOR3 distanceVector = Core::LocalPlayer->Position - player->Position;
	int distance = (int)D3DXVec3Length(&distanceVector);
	int alphaColor = Core::Target == player ? 255 : (1 - (distance / (HackConfig::PLAYER_MAX_DISTANCE * 1.0f))) * 255;

	if (distance < HackConfig::PLAYER_MAX_DISTANCE && player->Dead == 0) {
		ESP::NearPlayersCountAux++;

		textColor = HL::Draw::ToAlphaColor(player->Reputation <= -10 ? HL::Color::RED 
			: player->Reputation >= 10 ? HL::Color::LIME 
			: HL::Color::WHITE, alphaColor);

		Core::Radar->DrawRadarPoint(Core::LocalPlayer->Position, player->Position, Core::LocalPlayer->Yaw, HL::Draw::ToAlphaColor(textColor, 255));
	}

	if ((GameFunctions::WorldToScreen(player->Position, &screenPosition) 
		|| GameFunctions::WorldToScreen(player->GetBonePosition(10), &screenPosition)) 
		&& distance < HackConfig::PLAYER_MAX_DISTANCE) {

		char name[200];
		int yPadding = 5;
		DWORD deadColor = HL::Draw::ToAlphaColor(HL::Color::GRAY, alphaColor);
		DWORD skeletonColor = HL::Draw::ToAlphaColor(HL::Color::BLUE, alphaColor);
		DWORD skeletonAimColor = HL::Draw::ToAlphaColor(HL::Color::RED, alphaColor);

		if (player->Dead == 1 && Core::Menu->MenuItems[Menu::PlayerInfo].Value.BoolValue) {
			player->GetName(name);

			Core::Draw->DrawString(screenPosition.x, screenPosition.y + yPadding, deadColor, true, "[DEAD]%s [%im]", name, distance);

			return;
		} else {
			if (Core::Menu->MenuItems[Menu::PlayerInfo].Value.BoolValue
				&& (Core::Menu->MenuItems[Menu::OptimizedPlayerInfo].Value.BoolValue && (ESP::NearPlayersCount <= 20 || Core::Target == player)
				|| !Core::Menu->MenuItems[Menu::OptimizedPlayerInfo].Value.BoolValue)) {

				#pragma region Player Name, Distance and Health
				ESP::DrawHealthBar(screenPosition.x, screenPosition.y + yPadding, player->GetHeath(), alphaColor);
				yPadding += 5;

				player->GetName(name);

				if (player->AggressorFlag == 1) {
					sprintf(name, "%s [%im] [AGGRESSOR]", name, distance);
				} else {
					sprintf(name, "%s [%im]", name, distance);
				}
				yPadding += 8;
				#pragma endregion

				#pragma region Player Equips
				if (player->WeaponPrimary && player->WeaponPrimary->Armory) {
					sprintf(name, "%s\n%s", name, player->WeaponPrimary->Armory->Name);
					yPadding += 8;
				}

				if (player->WeaponSecondary && player->WeaponSecondary->Armory) {
					sprintf(name, "%s\n%s", name, player->WeaponSecondary->Armory->Name);
					yPadding += 8;
				}

				GameClasses::BaseItemConfig* backpack = GameFunctions::GetItemConfig(player->BackpackId);

				if (backpack) {
					sprintf(name, "%s\n%s", name, backpack->StoreName);
					yPadding += 8;
				}
				#pragma endregion

				Core::Draw->DrawString(screenPosition.x, screenPosition.y + yPadding, textColor, name, true);
			}

			#pragma region Player Skeleton
			if (Core::Menu->MenuItems[Menu::PlayerSkeleton].Value.BoolValue) {
				if (Core::Target == player) {
					distanceVector = Core::Target->GetHeadPosition() - Core::LocalPlayer->ViewTargetPosition;
					distance = (int)D3DXVec3Length(&distanceVector);

					if (distance <= 0.1f) {
						skeletonAimColor = HL::Draw::ToAlphaColor(HL::Color::LIME, alphaColor);
					}
					ESP::DrawPlayerSkeleton(player, skeletonAimColor);
				} else {
					ESP::DrawPlayerSkeleton(player, skeletonColor);
				}
			}
			#pragma endregion
		}

		#pragma region Get Spoted Player
		if (!HackEngine::IsTargetLocked) {
			D3DXVECTOR3 screenHeadPosition;
			D3DXVECTOR2 screenCenter = GameFunctions::GetScreenCenter();

			GameFunctions::WorldToScreen(player->GetHeadPosition(), &screenHeadPosition);
			float distanceToAim = sqrt(pow(screenHeadPosition.y - screenCenter.y, 2) + pow(screenHeadPosition.x - screenCenter.x, 2));

			if ((ESP::ClosestPlayerDistanceToAim == -1 || ESP::ClosestPlayerDistanceToAim >= distanceToAim)
				&& distanceToAim >= 0
				&& distanceToAim <= 200
				&& !KeyCode::IsKeyHolding(VK_RBUTTON)) {
				ESP::ClosestPlayerDistanceToAim = distanceToAim;
				ESP::Target = player;
			} else if (Core::Target == player && !KeyCode::IsKeyHolding(VK_RBUTTON)) {
				ESP::Target = NULL;
				Core::Target = NULL;
			}
		}
		#pragma endregion

	} else if (Core::Target == player && !KeyCode::IsKeyHolding(VK_RBUTTON) && !HackEngine::IsTargetLocked) {
		ESP::Target = NULL;
		Core::Target = NULL;
	}
}

void ESP::DrawZombie(GameClasses::Zombie* zombie) {
	if (zombie->ZombieState == GameObjectEnums::ZombieState::zDead) {
		return;
	}

	D3DXVECTOR3 screenPosition;
	D3DXVECTOR3 distanceVector = Core::LocalPlayer->Position - zombie->Position;
	int distance = (int)D3DXVec3Length(&distanceVector);

	if (zombie->ZombieState == GameObjectEnums::ZombieState::zChasing && distance < HackConfig::ZOMBIE_MAX_DISTANCE) {
		ESP::AggroZombieCount++;
	}

	if (GameFunctions::WorldToScreen(zombie->Position, &screenPosition) && distance < HackConfig::ZOMBIE_MAX_DISTANCE) {
		int alphaColor = (1 - (distance / (HackConfig::ZOMBIE_MAX_DISTANCE * 1.0f))) * 255;
		DWORD color = HL::Draw::ToAlphaColor(HL::Color::WHITE, alphaColor);

		#pragma region Zombie Position
		if (Core::Menu->MenuItems[Menu::ZombieInfo].Value.BoolValue) {
			Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, true, "Zombie [%dm]", distance);
		}
		#pragma endregion

		#pragma region Zombie Skeleton
		if (Core::Menu->MenuItems[Menu::ZombieSkeleton].Value.BoolValue) {
			ESP::DrawZombieSkeleton(zombie, color);
		}
		#pragma endregion
	}
}

void ESP::DrawObject(GameClasses::GameObject* object) {
	if (!Core::Menu->MenuItems[Menu::ItemInfo].Value.BoolValue) {
		return;
	}

	D3DXVECTOR3 screenPosition;
	D3DXVECTOR3 distanceVector = Core::LocalPlayer->Position - object->Position;
	int distance = (int)D3DXVec3Length(&distanceVector);

	static D3DXVECTOR3 originalPosition = D3DXVECTOR3(0, 0, 0);
	static D3DXVECTOR3 newPosition = D3DXVECTOR3(0, 0, 0);

	if (object->ItemId == 1 && strcmp(object->Name, "STORE") == 0) {
		if (KeyCode::IsKeyHolding(VK_F8) && originalPosition.x == 0) {
			originalPosition = object->Position;
			newPosition = Core::LocalPlayer->Position;

			object->SetPosition(newPosition);
		} else if (!KeyCode::IsKeyHolding(VK_F8) && newPosition.x != 0) {
			object->SetPosition(originalPosition);
			
			originalPosition = D3DXVECTOR3(0, 0, 0);
			newPosition = D3DXVECTOR3(0, 0, 0);
		}
	}

	if (GameFunctions::WorldToScreen(object->Position, &screenPosition) && distance < HackConfig::OBJECT_MAX_DISTANCE) {
		DWORD color;
		bool isSpotted = false;
		D3DXVECTOR2 screenCenter = GameFunctions::GetScreenCenter();
		
		if (abs(screenPosition.x - screenCenter.x) < 100
			&& abs(screenPosition.y - screenCenter.y) < 100
			&& distance < 4) {
			isSpotted = true;
		}

		GameClasses::BaseItemConfig* itemConfig = GameFunctions::GetItemConfig(object->ItemId);

		if (itemConfig) {
			switch (itemConfig->Category) {
				case GameObjectEnums::StoreCategories::ASR:
				case GameObjectEnums::StoreCategories::Grenade:
				case GameObjectEnums::StoreCategories::HG:
				case GameObjectEnums::StoreCategories::MG:
				case GameObjectEnums::StoreCategories::SHTG:
				case GameObjectEnums::StoreCategories::SMG:
				case GameObjectEnums::StoreCategories::SNP:
					color = HL::Color::DARK_PURPLE;
					break;
				case GameObjectEnums::StoreCategories::Armor:
				case GameObjectEnums::StoreCategories::Backpack:
				case GameObjectEnums::StoreCategories::Helmet:
				case GameObjectEnums::StoreCategories::HeroPackage:
				case GameObjectEnums::StoreCategories::FPSAttachment:
				case GameObjectEnums::StoreCategories::ItemSkin:
				case GameObjectEnums::StoreCategories::ItemBox:
				case GameObjectEnums::StoreCategories::Bullets:
				case GameObjectEnums::StoreCategories::LootBox:
					color = HL::Color::ORANGE;
					break;
				case GameObjectEnums::StoreCategories::Medicines:
					color = HL::Color::GREEN;
					break;
				case GameObjectEnums::StoreCategories::Traps:
					color = HL::Color::BLUE;
					break;
				case GameObjectEnums::StoreCategories::Melee:
				case GameObjectEnums::StoreCategories::Food:
				case GameObjectEnums::StoreCategories::Water:
				case GameObjectEnums::StoreCategories::CraftRecipe:
				case GameObjectEnums::StoreCategories::Fuel:
					color = HL::Draw::ToAlphaColor(HL::Color::WHITE, 150);
					break;
				default:
					return;
			}

			if (itemConfig->Category == GameObjectEnums::StoreCategories::Backpack) {
				D3DXVECTOR3 backPackCorrection = object->Position;
				backPackCorrection.y += 1;
				GameFunctions::WorldToScreen(backPackCorrection, &screenPosition);
			}

			if (isSpotted) {
				color = HL::Color::YELLOW;
			}

			if (object->Name) {
				if (object->StackQuantity > 1) {
					Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, true, "%s [%dm] %dx", object->Name, distance, object->StackQuantity);
				} else {
					Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, true, "%s [%dm]", object->Name, distance);
				}
			} else if (itemConfig->StoreName) {
				Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, true, "%s [%dm]", itemConfig->StoreName, distance);
			} else {
				Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, true, "Item [%dm] Category:%d", distance, itemConfig->Category);
			}
		} else {
			char name[60];

			switch (object->ObjectType) {
				case GameObjectEnums::ObjectType::Trap:
					color = HL::Color::RED;
					sprintf(name, "TRAP [%dm]", distance);
					isSpotted = false;
					break;
				case GameObjectEnums::ObjectType::DroppedItem:
					if (strcmp("LOOT BACKPACK", object->Name) == 0) {
						color = HL::Color::DARK_PURPLE;

						D3DXVECTOR3 backPackCorrection = object->Position;
						backPackCorrection.y += 1;
						GameFunctions::WorldToScreen(backPackCorrection, &screenPosition);

						sprintf(name, "LOOT [%dm]", distance);
					} else {
						if ((object->ItemId == 0 || object->ItemId == 100) && strcmp(object->Name, "Generator") == 0) {
							switch (object->GeneratorStatus) {
								case 2:
									color = HL::Color::YELLOW;
									sprintf(name, "Generator, need repair");
									break;
								case 3:
									color = HL::Color::YELLOW;
									sprintf(name, "Generator, need fuel");
									break;
								case 4:
									color = HL::Color::LIME;
									sprintf(name, "Generator, working fine");
									break;
								default:
									color = HL::Color::ORANGE;
									sprintf(name, "Generator, need repair and fuel");
									break;
							}
						} else {
							sprintf(name, "%s [%dm]", object->Name, distance, object);
						}
					}
					break;
				default:
					break;
			}

			if (isSpotted) {
				color = HL::Color::YELLOW;
			}

			Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, name, true);
		}

		if (isSpotted && KeyCode::IsKeyHolding(KeyCode::KeyG)) {
			object->SetPosition(Core::LocalPlayer->Position);
		}
	}
}

void ESP::DrawObjectDebug(GameClasses::GameObject* object) {
	D3DXVECTOR3 screenPosition;
	D3DXVECTOR3 distanceVector = Core::LocalPlayer->Position - object->Position;
	int distance = (int)D3DXVec3Length(&distanceVector);

	if (GameFunctions::WorldToScreen(object->Position, &screenPosition) && distance < HackConfig::OBJECT_MAX_DISTANCE) {
		DWORD color = HL::Color::GREEN;

		Core::Draw->DrawString(screenPosition.x, screenPosition.y + 5, color, true, "Type:%#09X [%dm] %p", object->ObjectType, distance, object);
	}
}

void ESP::DrawPlayerSkeleton(GameClasses::Player* player, DWORD color) {
	D3DXVECTOR2 lines[9];
	int lineCount;

	#pragma region Arms
	int arms[9] = {
		35, 34, 33, 32, 8, 11, 12, 13, 14
	};

	lineCount = 0;

	for (int i = 0; i < 9; i++) {
		D3DXVECTOR3 bone = player->GetBonePosition(arms[i]);
		D3DXVECTOR3 bonePosition;

		if (GameFunctions::WorldToScreen(bone, &bonePosition)) {
			lines[lineCount] = D3DXVECTOR2(bonePosition.x, bonePosition.y);
			lineCount++;
		}
	}

	if (lineCount >= 2) {
		Core::Draw->DrawLines(lines, lineCount, color);
	}
	#pragma endregion

	#pragma region Trunk
	int trunk[7]{
		1, 2, 3, 4, 8, 9, 10
	};

	lineCount = 0;

	for (int i = 0; i < 7; i++) {
		D3DXVECTOR3 bone = player->GetBonePosition(trunk[i]);
		D3DXVECTOR3 bonePosition;

		if (GameFunctions::WorldToScreen(bone, &bonePosition)) {
			lines[lineCount] = D3DXVECTOR2(bonePosition.x, bonePosition.y);
			lineCount++;
		}
	}

	if (lineCount >= 2) {
		Core::Draw->DrawLines(lines, lineCount, color);
	}
	#pragma endregion

	#pragma region Legs
	int legs[8]{
		62, 61, 60, 58, 54, 55, 56, 57
	};

	lineCount = 0;

	for (int i = 0; i < 8; i++) {
		D3DXVECTOR3 bone = player->GetBonePosition(legs[i]);
		D3DXVECTOR3 bonePosition;

		if (GameFunctions::WorldToScreen(bone, &bonePosition)) {
			lines[lineCount] = D3DXVECTOR2(bonePosition.x, bonePosition.y);
			lineCount++;
		}
	}

	if (lineCount >= 2) {
		Core::Draw->DrawLines(lines, lineCount, color);
	}
	#pragma endregion
}

void ESP::DrawZombieSkeleton(GameClasses::Zombie* zombie, DWORD color) {
	D3DXVECTOR2 lines[9];
	int lineCount;

	if (zombie->ZombieState == GameObjectEnums::ZombieState::zDead) {
		return;
	}

	#pragma region Arms
	int arms[9] = {
		31, 30, 29, 28, 5, 7, 8, 9, 10
	};

	lineCount = 0;

	for (int i = 0; i < 9; i++) {
		D3DXVECTOR3 bone = zombie->GetBonePosition(arms[i]);
		D3DXVECTOR3 bonePosition;

		if (GameFunctions::WorldToScreen(bone, &bonePosition)) {
			lines[lineCount] = D3DXVECTOR2(bonePosition.x, bonePosition.y);
			lineCount++;
		}
	}

	if (lineCount >= 2) {
		Core::Draw->DrawLines(lines, lineCount, color);
	}
	#pragma endregion

	#pragma region Trunk
	int trunk[6]{
		1, 2, 3, 4, 5, 6
	};

	lineCount = 0;

	for (int i = 0; i < 6; i++) {
		D3DXVECTOR3 bone = zombie->GetBonePosition(trunk[i]);
		D3DXVECTOR3 bonePosition;

		if (GameFunctions::WorldToScreen(bone, &bonePosition)) {
			lines[lineCount] = D3DXVECTOR2(bonePosition.x, bonePosition.y);
			lineCount++;
		}
	}

	if (lineCount >= 2) {
		Core::Draw->DrawLines(lines, lineCount, color);
	}
	#pragma endregion

	#pragma region Legs
	int legs[8]{
		56, 55, 54, 53, 58, 59, 60 ,61
	};

	lineCount = 0;

	for (int i = 0; i < 8; i++) {
		D3DXVECTOR3 bone = zombie->GetBonePosition(legs[i]);
		D3DXVECTOR3 bonePosition;

		if (GameFunctions::WorldToScreen(bone, &bonePosition)) {
			lines[lineCount] = D3DXVECTOR2(bonePosition.x, bonePosition.y);
			lineCount++;
		}
	}

	if (lineCount >= 2) {
		Core::Draw->DrawLines(lines, lineCount, color);
	}
	#pragma endregion
}

void ESP::DrawHealthBar(int x, int y, float health, int alpha) {
	x -= 25;
	DWORD color;

	if (health > 90) {
		color = HL::Color::GREEN;
	} else if (health > 60) {
		color = HL::Color::YELLOW;
	} else if (health > 30) {
		color = HL::Color::ORANGE;
	} else {
		color = HL::Color::RED;
	}

	Core::Draw->DrawFilledRect(x, y, 50, 2, HL::Draw::ToAlphaColor(HL::Color::BLACK, alpha));
	Core::Draw->DrawFilledRect(x, y, health / 2, 2, HL::Draw::ToAlphaColor(color, alpha));
}
#pragma endregion