#pragma once
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include <d3d9.h>
#include <d3dx9.h>

namespace GameClasses {
	class Bone {
	public:
		char* Name;					//0x00  
		char _0x0004[136];			//0x04
		D3DXMATRIX BoneMatrix;		//0x8C  
		D3DXMATRIX SomeMatrix;		//0xCC  
	};

	class BoneInfo {
	public:
		char _0x0000[12];			//0x00
		int OneNum;					//0x0C 
		char _0x0010[8];			//0x10
		Bone*   Bones;				//0x18 
	};

	class Skeleton {
	public:
		char _0x0000[28];			//0x00
		BoneInfo* BoneInfo;			//0x1C 
	};
}