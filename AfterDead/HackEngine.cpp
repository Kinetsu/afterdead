#include "HackEngine.h"
#include "Core.h"
#include "GameFunctions.h"
#include "KeyCode.h"
#include "GameObjectEnums.h"

#include <math.h>
#include <Windows.h>

#pragma region Private
bool HackEngine::IsTeleporting = false;
clock_t HackEngine::GhostModeStart = NULL;
D3DXVECTOR3 HackEngine::SafePosition = D3DXVECTOR3(1756.61, 172.81, 694.45);

HL::LagSwitch* HackEngine::LagSwitcher = new HL::LagSwitch("AfterDeadLS" , "aftermath" , Core::GetCurrentPath());
int HackEngine::NoClipSpeed = atoi(Core::ReadConfig("Configs", "NoClipSpeed", "74"));
int HackEngine::SpeedHackSpeed = atoi(Core::ReadConfig("Configs", "SpeedHack", "38"));
int HackEngine::TeleportLocationIndex = 0;
bool HackEngine::IsGhostJumping = false;
bool HackEngine::IsTargetLocked = false;
bool HackEngine::IsShowingTeleportScreen = false;

HackEngine::TeleportLocation HackEngine::TeleportLocations[11] = {
	HackEngine::TeleportLocation("[1] Mountainpass [SAFE]", D3DXVECTOR3(1394.82, 152.34, 656.14)),
	HackEngine::TeleportLocation("[2] Green Valley [SAFE]", D3DXVECTOR3(258.18, 140.01, 1214.98)),
	HackEngine::TeleportLocation("[3] Pinevale", D3DXVECTOR3(1871.0, 173.64, 678.01)),
	HackEngine::TeleportLocation("[4] Craigstone", D3DXVECTOR3(870.37, 141.64, 1146.79)),
	HackEngine::TeleportLocation("[5] Gainsville", D3DXVECTOR3(335.91, 168.33, 1810.36)),
	HackEngine::TeleportLocation("[6] Rockden Base", D3DXVECTOR3(1809.28, 153.50, 1365.59)),
	HackEngine::TeleportLocation("[7] Greenport Village", D3DXVECTOR3(1638.48, 139.22, 1658.08)),
	HackEngine::TeleportLocation("[8] Sindal Airfield", D3DXVECTOR3(302.46, 171.63, 247.78)),
	HackEngine::TeleportLocation("[9] Oakfort", D3DXVECTOR3(1751.93, 119.93, 157.18)),
	HackEngine::TeleportLocation("[0] Last Location", D3DXVECTOR3(0, 0, 0)),
	HackEngine::TeleportLocation("RESERVED FOR HACK", D3DXVECTOR3(0, 0, 0))
};

D3DXVECTOR2 HackEngine::GetAnglesTo(D3DXVECTOR2 targetScreenPosition) {
	D3DXVECTOR2 result;
	D3DXVECTOR2 screenCenter = GameFunctions::GetScreenCenter();
	D3DXVECTOR2 normalizedScreenCenter = screenCenter;
	D3DXVec2Normalize(&normalizedScreenCenter, &normalizedScreenCenter);

	#pragma region Getting Yaw
	D3DXVECTOR2 bottomRight(targetScreenPosition.x, screenCenter.y);
	D3DXVec2Normalize(&bottomRight, &bottomRight);

	float yawDotProduct = D3DXVec2Dot(&normalizedScreenCenter, &bottomRight);
	bool turnLeft = D3DXToDegree(atan2f(bottomRight.y - normalizedScreenCenter.y, bottomRight.x - normalizedScreenCenter.x)) < 0.0f;
	float yawToTurn = D3DXToDegree(acos(min(yawDotProduct, 1.0f)));
	#pragma endregion

	#pragma region Getting Pitch
	D3DXVECTOR2 topLeft(screenCenter.x, targetScreenPosition.y);
	D3DXVec2Normalize(&topLeft, &topLeft);

	float pitchDotProduct = D3DXVec2Dot(&normalizedScreenCenter, &topLeft);
	bool turnDown = D3DXToDegree(atan2f(topLeft.y - normalizedScreenCenter.y, topLeft.x - normalizedScreenCenter.x)) < 0.0f;
	float pitchToTurn = D3DXToDegree(acos(min(pitchDotProduct, 1.0f)));
	#pragma endregion

	if (yawToTurn < 0.05f) {
		yawToTurn = 0;
	}

	if (pitchToTurn < 0.05f) {
		pitchToTurn = 0;
	}

	result.x = turnLeft ? -yawToTurn : yawToTurn;
	result.y = turnDown ? pitchToTurn : -pitchToTurn;

	return result;
}
#pragma endregion

#pragma region Public
void HackEngine::NoClip(int speed) {
	float yaw = Core::LocalPlayer->Yaw - 90;
	float fy = D3DXToRadian(yaw);
	float fc = cos(fy);
	float fs = sin(fy);
	D3DXVECTOR3 movement;

	movement.z = -(fs * (speed / 1000.0f));
	movement.y = 0;
	movement.x = -(fc * (speed / 1000.0f));

	D3DXVECTOR3 finalPosition = Core::LocalPlayer->Position + movement;

	Core::LocalPlayer->SetPosition(finalPosition);
}

void HackEngine::ElevatorUp() {
	D3DXVECTOR3 movement = D3DXVECTOR3(0, 0.1, 0);
	D3DXVECTOR3 finalPosition = Core::LocalPlayer->Position + movement;

	Core::LocalPlayer->Position = finalPosition;
}

void HackEngine::ElevatorDown() {
	D3DXVECTOR3 movement = D3DXVECTOR3(0, -0.1, 0);
	D3DXVECTOR3 finalPosition = Core::LocalPlayer->Position + movement;

	Core::LocalPlayer->Position = finalPosition;
}

void HackEngine::GhostModeSwitch() {
	HackEngine::LagSwitcher->Switch();

	if (HackEngine::LagSwitcher->IsOn) {
		HackEngine::GhostModeStart = clock();
	} else {
		HackEngine::GhostModeStart = NULL;
	}
}

void HackEngine::SuperJump() {
	Core::LocalPlayer->JumpVelocity = 17.0f;
}

void HackEngine::SuperGun() {
	GameClasses::Weapon* weapon = Core::LocalPlayer->WeaponPrimary;

	if (weapon && weapon->Armory) {
		weapon->Armory->Decay = 999.0f;
		weapon->Armory->SpreadCurve = 0.0f;
		weapon->Armory->SpreadMult = 0.0f;
		weapon->Armory->SpreadLeft = 0.0f;
		weapon->Armory->SpreadRight = 0.0f;
		weapon->Armory->SpreadUp = 0.0f;
		weapon->Armory->SpreadDown = 0.0f;
		weapon->Armory->Recoil = 0.0f;
		weapon->Armory->RecoilLeft = 0.0f;
		weapon->Armory->RecoilRight = 0.0f;
		weapon->Armory->RecoilUp = 0.0f;
		weapon->Armory->RecoilDown = 0.0f;
		weapon->Armory->FireDelay = 0.1f;
		weapon->Armory->ReloadTime = 0.0f;
		weapon->Armory->Speed = 9999.0f;
		weapon->Armory->RateOfFire = 999.0f;

		Core::LocalPlayer->WeaponsSlot[0].Durability = 10000;
	}

	weapon = Core::LocalPlayer->WeaponSecondary;

	if (weapon && weapon->Armory) {
		weapon->Armory->Decay = 999.0f;
		weapon->Armory->SpreadCurve = 0.0f;
		weapon->Armory->SpreadMult = 0.0f;
		weapon->Armory->SpreadLeft = 0.0f;
		weapon->Armory->SpreadRight = 0.0f;
		weapon->Armory->SpreadUp = 0.0f;
		weapon->Armory->SpreadDown = 0.0f;
		weapon->Armory->Recoil = 0.0f;
		weapon->Armory->RecoilLeft = 0.0f;
		weapon->Armory->RecoilRight = 0.0f;
		weapon->Armory->RecoilUp = 0.0f;
		weapon->Armory->RecoilDown = 0.0f;
		weapon->Armory->ReloadTime = 0.0f;
		weapon->Armory->Speed = 9999.0f;
		weapon->Armory->RateOfFire = 999.0f;

		Core::LocalPlayer->WeaponsSlot[1].Durability = 10000;

		switch (weapon->Armory->ItemID) {
			case 102817:
			case 102821:
				weapon->Armory->FireDelay = 0.325f;
				break;
			case 102830:
				weapon->Armory->FireDelay = 0.275f;
				break;
			default:
				weapon->Armory->FireDelay = 0.001f;
				break;
		}
	}

	weapon = Core::LocalPlayer->Fists;

	if (weapon && weapon->Armory) {
		weapon->Armory->FireDelay = 0.001f;
		weapon->Armory->Speed = 9999.0f;
		weapon->Armory->Decay = 999.0f;
		weapon->Armory->RateOfFire = 999.0f;
	}
}

void HackEngine::AutoHeal() {
	float lifeToRecover = 100 - Core::LocalPlayer->GetHeath();

	if (lifeToRecover < 20) {
		return;
	}

	GameClasses::Medicine* quickSlots[] = {
		(GameClasses::Medicine*)GameFunctions::GetItemConfig(Core::LocalPlayer->QuickSlots[0].ItemId),
		(GameClasses::Medicine*)GameFunctions::GetItemConfig(Core::LocalPlayer->QuickSlots[1].ItemId),
		(GameClasses::Medicine*)GameFunctions::GetItemConfig(Core::LocalPlayer->QuickSlots[2].ItemId),
		(GameClasses::Medicine*)GameFunctions::GetItemConfig(Core::LocalPlayer->QuickSlots[3].ItemId)
	};

	float nearestLifeToRecovery = 100.0f;
	int keyCode = 0;

	for (int i = 0; i < 4; i++) {
		if (!quickSlots[i] || quickSlots[i]->Category != GameObjectEnums::StoreCategories::Medicines) {
			continue;
		}

		if (abs(quickSlots[i]->Health - lifeToRecover) < nearestLifeToRecovery) {
			switch (i) {
				case 0:
					keyCode = KeyCode::Key3;
					break;
				case 1:
					keyCode = KeyCode::Key4;
					break;
				case 2:
					keyCode = KeyCode::Key5;
					break;
				case 3:
					keyCode = KeyCode::Key6;
					break;
			}
		}
	}

	if (keyCode != 0) {
		INPUT input;

		input.type = INPUT_KEYBOARD;
		input.ki.time = 0;
		input.ki.wVk = 0;
		input.ki.dwExtraInfo = 0;

		input.ki.dwFlags = KEYEVENTF_SCANCODE;
		input.ki.wScan = MapVirtualKey(keyCode, MAPVK_VK_TO_VSC);
		SendInput(1, &input, sizeof(INPUT));

		input.ki.dwFlags = KEYEVENTF_SCANCODE | KEYEVENTF_KEYUP;
		SendInput(1, &input, sizeof(INPUT));
	}
}

void HackEngine::Aimbot() {
	if (!GameFunctions::IsValidPlayer(Core::Target)) {
		return;
	}

	D3DXVECTOR3 screenHeadPosition;

	GameFunctions::WorldToScreen(Core::Target->GetHeadPosition(), &screenHeadPosition);
	D3DXVECTOR2 anglesToTurn = HackEngine::GetAnglesTo(D3DXVECTOR2(screenHeadPosition.x, screenHeadPosition.y));

	Core::LocalPlayer->Yaw += anglesToTurn.x;
	Core::LocalPlayer->Pitch += anglesToTurn.y * 0.5f;
}

void HackEngine::Disconnect() {
	GameFunctions::SetConnected(false);
}

void HackEngine::TeleportToTargetPosition() {
	if (!HackEngine::IsTeleporting) {
		HackEngine::IsTeleporting = true;
		SetThreadPriority(CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)HackEngine::TeleportToTargetPositionThread, NULL, NULL, NULL), THREAD_PRIORITY_HIGHEST);
	}
}

void HackEngine::TeleportToTargetPositionThread() {
	while (KeyCode::IsKeyHolding(KeyCode::KeyH) 
		&& GameFunctions::IsValidPlayer(Core::Target)
		&& Core::Target->Dead == 0
		&& Core::LocalPlayer->Dead == 0
		&& HackEngine::IsTargetLocked) {

		#pragma region Calculate front position
		float yaw = Core::LocalPlayer->Yaw - 90;
		float fy = D3DXToRadian(yaw);
		float fc = cos(fy);
		float fs = sin(fy);
		D3DXVECTOR3 movement;

		movement.z = -fs * 1.25f;
		movement.y = 0;
		movement.x = -fc * 1.25f;
		#pragma endregion

		D3DXVECTOR3 finalPosition = Core::Target->Position - movement;

		if (Core::LocalPlayer->Position.x != finalPosition.x
			|| Core::LocalPlayer->Position.y != finalPosition.y
			|| Core::LocalPlayer->Position.z != finalPosition.z) {
			Core::LocalPlayer->Position = finalPosition;
		}
	}

	HackEngine::IsTeleporting = false;
}

void HackEngine::TeleportToDeadPosition() {
	if (!HackEngine::IsTeleporting) {
		HackEngine::IsTeleporting = true;
		SetThreadPriority(CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)HackEngine::TeleportToDeadPositionThread, NULL, NULL, NULL), THREAD_PRIORITY_HIGHEST);
	}
}

void HackEngine::TeleportToDeadPositionThread() {
	while (Core::GameWorld->Inited
		&& GameFunctions::IsConnected() 
		&& Core::LocalPlayer->Dead == 1) {

		if (Core::LocalPlayer->Position.x != Core::LocalPlayer->DeadPosition.x
			|| Core::LocalPlayer->Position.y != Core::LocalPlayer->DeadPosition.y
			|| Core::LocalPlayer->Position.z != Core::LocalPlayer->DeadPosition.z) {
			D3DXVECTOR3 offset = D3DXVECTOR3(0, 1, 0);
			Core::LocalPlayer->Position = Core::LocalPlayer->DeadPosition + offset;
		}
	}

	HackEngine::IsTeleporting = false;
}

void HackEngine::TeleportToSafePosition(){
	if (!HackEngine::IsTeleporting) {
		HackEngine::IsTeleporting = true;
		SetThreadPriority(CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)HackEngine::TeleportToSafePositionThread, NULL, NULL, NULL), THREAD_PRIORITY_HIGHEST);
	}
}

void HackEngine::TeleportToSafePositionThread() {
	while (KeyCode::IsKeyHolding(VK_F5)
		&& Core::GameWorld->Inited
		&& GameFunctions::IsConnected()
		&& Core::LocalPlayer->Dead == 0) {

		if (Core::LocalPlayer->Position.x != HackEngine::SafePosition.x
			|| Core::LocalPlayer->Position.y != HackEngine::SafePosition.y
			|| Core::LocalPlayer->Position.z != HackEngine::SafePosition.z) {
			Core::LocalPlayer->Position = HackEngine::SafePosition;
		}
	}

	HackEngine::IsTeleporting = false;
}

void HackEngine::TeleportToPosition() {
	D3DXVECTOR3 position = HackEngine::TeleportLocations[HackEngine::TeleportLocationIndex].Position;
	HackEngine::TeleportLocations[10].Position = position;

	if (!HackEngine::IsTeleporting && position.x != 0) {
		HackEngine::IsTeleporting = true;
		
		HackEngine::TeleportLocations[9].Position = Core::LocalPlayer->Position;
		HackEngine::TeleportLocationIndex = 9;

		SetThreadPriority(CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)HackEngine::TeleportToPositionThread, NULL, NULL, NULL), THREAD_PRIORITY_HIGHEST);
	}
}

void HackEngine::TeleportToPositionThread() {
	D3DXVECTOR3 pos = HackEngine::TeleportLocations[10].Position;
	clock_t begin = clock();
	
	HackEngine::IsShowingTeleportScreen = true;

	while (Core::GameWorld->Inited
		&& GameFunctions::IsConnected()
		&& Core::LocalPlayer->Dead == 0
		&& (clock() - begin) / CLOCKS_PER_SEC < 0.5) {

		if (Core::LocalPlayer->Position.x != pos.x
			|| Core::LocalPlayer->Position.y != pos.y
			|| Core::LocalPlayer->Position.z != pos.z) {
			Core::LocalPlayer->Position = pos;
		} else {
			Sleep(1);
		}
	}

	pos = D3DXVECTOR3(0, 1, 0);

	while (Core::GameWorld->Inited
		&& GameFunctions::IsConnected()
		&& Core::LocalPlayer->Dead == 0
		&& (clock() - begin) / CLOCKS_PER_SEC < 1.5) {

		if (Core::LocalPlayer->Position.x != pos.x
			|| Core::LocalPlayer->Position.y != pos.y
			|| Core::LocalPlayer->Position.z != pos.z) {
			Core::LocalPlayer->Position = pos;
		}
	}

	while (Core::LocalPlayer->IsOnGround == 0 && (clock() - begin) / CLOCKS_PER_SEC < 5) {
		Sleep(100);
	}

	if ((clock() - begin) / CLOCKS_PER_SEC >= 5) {
		Core::LocalPlayer->Position = HackEngine::SafePosition;
	}

	HackEngine::IsShowingTeleportScreen = false;
	HackEngine::IsTeleporting = false;
}

void HackEngine::PlayerMagnet() {
	if (!GameFunctions::IsValidPlayer(Core::Target)) {
		return;
	}

	D3DXVECTOR3 distanceVector = Core::LocalPlayer->Position - Core::Target->Position;
	int distance = (int)D3DXVec3Length(&distanceVector);

	if (distance > 3) {
		return;
	}

	#pragma region Calculate front position
	float yaw = Core::LocalPlayer->Yaw - 90;
	float fy = D3DXToRadian(yaw);
	float fc = cos(fy);
	float fs = sin(fy);
	D3DXVECTOR3 movement;

	movement.z = -fs;
	movement.y = 0;
	movement.x = -fc;

	D3DXVECTOR3 finalPosition = Core::LocalPlayer->Position + movement;
	#pragma endregion

	Core::Target->Position = finalPosition;
}

float HackEngine::GetGhostModeElapsedTime() {
	if (HackEngine::LagSwitcher->IsOn && HackEngine::GhostModeStart) {
		clock_t current = clock();

		return float(current - HackEngine::GhostModeStart) / CLOCKS_PER_SEC;
	} else {
		return 0;
	}
}

void HackEngine::Release() {
	if (HackEngine::LagSwitcher->IsOn) {
		HackEngine::LagSwitcher->Switch();
	}
	HackEngine::LagSwitcher->Release();
}
#pragma endregion