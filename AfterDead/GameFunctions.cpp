#pragma comment(lib, "Draw.lib")

#include "GameFunctions.h"
#include "Core.h"

#include <Draw.h>

GameFunctions::tGetItemConfig GameFunctions::GetItemConfig = NULL;

bool GameFunctions::WorldToScreen(const D3DXVECTOR3& worldPosition, D3DXVECTOR3* screenPosition) {
	D3DXMATRIX projectionMatrix;
	D3DXMATRIX viewMatrix;
	D3DXMATRIX worldMatrix;
	D3DVIEWPORT9 viewport;

	D3DXMATRIX tempMatrix = *(D3DXMATRIX*)((DWORD)Core::GameRenderer + Offsets::ProjectionMatrix);

	if (tempMatrix.m[3][3] != 1.f) {
		projectionMatrix = tempMatrix;
		viewMatrix = *(D3DXMATRIX*)((DWORD)Core::GameRenderer + Offsets::ViewMatrix);
	}

	Core::Draw->Device->GetViewport(&viewport);
	D3DXMatrixIdentity(&worldMatrix);
	D3DXVec3Project(screenPosition, &worldPosition, &viewport, &projectionMatrix, &viewMatrix, &worldMatrix);

	GameClasses::GameRenderer::WindownsSize* windowSize = Core::GameRenderer->GetSize();

	return (screenPosition->z >= 1 || screenPosition->z < 0 ||
		screenPosition->x > windowSize->Width || screenPosition->x < 0 ||
		screenPosition->y > windowSize->Height || screenPosition->y < 0) == false;
}

D3DXVECTOR2 GameFunctions::GetScreenCenter() {
	D3DXVECTOR2 result;
	
	result.x = Core::GameRenderer->GetSize()->Width / 2;
	result.y = Core::GameRenderer->GetSize()->Height / 2;

	return result;
}

bool GameFunctions::IsValidPlayer(GameClasses::Player* player) {
	return player && player->NameLength > 0 && player->NameLength < 60;
}

bool GameFunctions::IsConnected() {
	DWORD* isConnected = (DWORD*)(((DWORD)Core::GameLogic) + Offsets::Disconnect);

	return *isConnected == 1;
}

void GameFunctions::SetConnected(bool connected) {
	DWORD* isConnected = (DWORD*)(((DWORD)Core::GameLogic) + Offsets::Disconnect);

	*isConnected = connected ? 1 : 0;
}