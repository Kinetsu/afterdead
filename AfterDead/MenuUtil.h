#pragma once

enum Menu {
	ESP = 0,
	PlayerSkeleton = 1,
	PlayerInfo = 2,
	OptimizedPlayerInfo = 3,
	ZombieSkeleton = 4,
	ZombieInfo = 5,
	ItemInfo = 6,

	CHEATING = 7,
	Aimbot = 8,
	InstantDisconnect = 9,
	GhostMode = 10,
	AutoHeal = 11,
	GoToSafePosition = 12,
	SuperJump = 13,
	GhostJump = 14,
	NoClip = 15,
	PlayerMagnet = 16,
	PlayerAutoMagnet = 17,
	SpeedHack = 18,
	TeleportToPlayer = 19,
	SuperBullet = 20,
	InfiniteStamina = 21,
	RespawnOnDeadPosition = 22,
	PlaceObjectsEverywere = 23,
	KeepTargetLockedOnDeath = 24
};

enum HUD {
	Server = 0,
	Ping = 1,
	Kills = 2,
	Deaths = 3,
	Reputation = 4,
	Health = 5,
	Backpack = 6,
	Weapon1 = 7,
	Weapon2 = 8,
	NoclipSpeed = 9,
	SpeedhackSpeed = 10,
	HudGhostJump = 11,
	AutoMagnet = 12,
	HudAutoHeal = 13,
	Compass = 14,
	Teleport = 15,
	AimbotTarget = 16
};