#pragma once

#include <Windows.h>

class Offsets {
public:
	static const DWORD LocalPlayer = 0xD3824;
	static const DWORD Disconnect = 0xD3828;
	static const DWORD ProjectionMatrix = 0x1E370;
	static const DWORD ViewMatrix = 0x1E2F0;
	static const DWORD ServerName = 0xD3840;
};