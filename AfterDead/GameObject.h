#pragma once

#include "Skeleton.h"
#include "String.h"

namespace GameClasses {
	struct InventoryItem {
		char pad_0x0000[0x8]; //0x0000
		int ItemId; //0x0008 
		int Quantity; //0x000C 
		int Ammo; //0x0010 
		char pad_0x0014[0x4]; //0x0014
		int Durability; //0x0018 
		char pad_0x001C[0x4C]; //0x001C
	};

	class GameObject {
	public:
		virtual void v1();
		virtual void v2();
		virtual void v3();
		virtual void v4();
		virtual void v5();
		virtual void v6();
		virtual void v7();
		virtual void v8();
		virtual void v9();
		virtual void v10();
		virtual void v11();
		virtual void v12();
		virtual void v13();
		virtual void v14();
		virtual void v15();
		virtual void v16();
		virtual void v17();
		virtual void SetPosition(const D3DXVECTOR3& pos);

		char _0x0004[0x8]; //0x0004
		D3DXVECTOR3 SomePosition; //0x000C 
		char pad_0x0018[0xC]; //0x0018
		D3DXVECTOR3 Position; //0x0024 
		char pad_0x0030[0x20]; //0x0030
		GameObject* PrevObject; //0x0050 
		GameObject* NextObject; //0x0054 
		char pad_0x0058[0x180]; //0x0058
		D3DXMATRIX BoneCoeff; //0x01D8 
		char pad_0x0218[0x28]; //0x0218
		int ObjectType; //0x0240 
		char pad_0x0244[0x4C]; //0x0244
		float SomeCounter; //0x0290 
		char pad_0x0294[0x174]; //0x0294
		char* Name; //0x0408 
		char* InteractionDescription; //0x040C 
		D3DXVECTOR3 SomePosition2; //0x0410 
		char pad_0x041C[0x4]; //0x041C
		int GeneratorStatus; //0x0420 
		int GeneratorRepair; //0x0424 
		int ItemId; //0x0428 
		int StackQuantity; //0x042C 
		char pad_0x0430[0x8]; //0x0430
		int Durability; //0x0438 
	};

	class BaseItemConfig {
	public:
		char _0x0000[4];		//0x0000
		int ItemID;				//0x0004 
		int Category;			//0x0008 
		char _0x000C[4];		//0x000C
		char* Description;		//0x0010 
		char* StoreIcon;		//0x0014 
		char* StoreName;		//0x0018 
		char _0x001C[4];		//0x001C
		float Weight;			//0x0020

		std::string GetFormattedName() {
			std::string result;
			result = String::ReplaceAll(this->StoreName, "$", "");
			result = String::ReplaceAll(result, "INB_", "");
			result = String::ReplaceAll(result, "_", " ");
			return result;
		}
	};

	class Medicine {
	public:
		char _0x0000[0x4];	//0x0000
		int ItemId;			//0x0004 
		int Category;		//0x0008 
		char _0x000C[0x4];	//0x000C
		char* Description;	//0x0010 
		char* StoreIcon;	//0x0014 
		char* StoreName;	//0x0018 
		char _0x001C[0x4];	//0x001C
		float Weight;		//0x0020 
		char _0x0024[0x20];	//0x0024
		int Health;			//0x0044 
		int Bleeding;		//0x0048 
		int ZombieCells;	//0x004C 
		int Feever;			//0x0050 
		int Poison;			//0x0054 
		int Infection;		//0x0058

		std::string GetFormattedName() {
			std::string result;
			result = String::ReplaceAll(this->StoreName, "$", "");
			result = String::ReplaceAll(result, "INB_", "");
			result = String::ReplaceAll(result, "_", " ");
			return result;
		}
	};

	class Armory {
	public:
		char _0x0000[4];		//0x0000
		int ItemID;				//0x0004 
		int Category;			//0x0008 
		char _0x000C[20];		//0x000C
		float Weight;			//0x0020 
		char _0x0024[60];		//0x0024
		char* Name;				//0x0060 
		char _0x0064[20];		//0x0064
		float Mass;				//0x0078 
		float Speed;			//0x007C 
		float Damage;			//0x0080 
		float Decay;			//0x0084
		char _0x0088[24];		//0x0088
		float ActiveReloadTick; //0x00A0
		char _0x00A4[4];		//0x00A4
		float SpreadCurve;		//0x00A8
		float SpreadMult;		//0x00AC
		float SpreadLeft;		//0x00B0
		float SpreadRight;		//0x00B4
		float SpreadUp;			//0x00B8
		float SpreadDown;		//0x00BC
		float Recoil;			//0x00C0
		float RecoilLeft;		//0x00C4
		float RecoilRight;		//0x00C8
		float RecoilUp;			//0x00CC
		float RecoilDown;		//0x00D0
		char _0x00D4[4];		//0x00D4
		float RateOfFire;		//0x00D8
		float FireDelay;		//0x00DC
		float ReloadTime;		//0x00E0
		float AggroRadius;		//0x00E4 

		std::string GetFormattedName() {
			std::string result;
			result = String::ReplaceAll(this->Name, "$", "");
			result = String::ReplaceAll(result, "INB_", "");
			result = String::ReplaceAll(result, "HomeBrew_", "");
			result = String::ReplaceAll(result, "_", " ");
			return result;
		}
	};

	class Weapon {
	public:
		char _0x0000[40];		//0x0000
		Armory* Armory;			//0x0028
		char _0x002C[424];
		int FireMode;			//0x01D4 4 = fullauto
	};
}