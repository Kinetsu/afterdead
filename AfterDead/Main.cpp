#include "Main.h"
#include "Core.h"

BOOL APIENTRY DllMain(HMODULE hModule, DWORD dwReason, LPVOID lpReserved) {
	switch (dwReason) {
		case DLL_PROCESS_ATTACH:
			Core::Log->Write("Attaching...");

			DisableThreadLibraryCalls(hModule);
			CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)Core::Init, NULL, NULL, NULL);

			break;
		case DLL_PROCESS_DETACH:
			Core::Log->Write("Detaching...");

			Core::IsRuning = false;

			Sleep(1000);

			break;
	}

	return TRUE;
}