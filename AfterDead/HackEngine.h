#pragma once
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")
#pragma comment(lib, "LagSwitch.lib")

#include <d3d9.h>
#include <d3dx9.h>
#include <LagSwitch.h>
#include <ctime>
#include "Player.h"

class HackEngine {
public:
	struct TeleportLocation {
		char* Name;
		D3DXVECTOR3 Position;

		TeleportLocation() {
			Name = "";
			Position = D3DXVECTOR3(0, 0, 0);
		}

		TeleportLocation(char* name, D3DXVECTOR3 position) {
			Name = name;
			Position = position;
		}
	};

private:
	static bool IsTeleporting;
	static clock_t GhostModeStart;
	static D3DXVECTOR3 SafePosition;

public:
	static HL::LagSwitch* LagSwitcher;
	static int NoClipSpeed;
	static int SpeedHackSpeed;
	static int TeleportLocationIndex;
	static bool IsGhostJumping;
	static bool IsTargetLocked;
	static bool IsShowingTeleportScreen;
	static TeleportLocation TeleportLocations[11];

private:
	static D3DXVECTOR2 GetAnglesTo(D3DXVECTOR2 targetScreenPosition);
	static void TeleportToTargetPositionThread();
	static void TeleportToDeadPositionThread();
	static void TeleportToSafePositionThread();
	static void TeleportToPositionThread();

public:
	static void NoClip(int speed = HackEngine::NoClipSpeed);
	static void ElevatorUp();
	static void ElevatorDown();

	static void GhostModeSwitch();
	static void SuperJump();
	static void SuperGun();
	static void AutoHeal();
	static void Aimbot();
	static void Disconnect();

	static void TeleportToTargetPosition();
	static void TeleportToDeadPosition();
	static void TeleportToSafePosition();
	static void TeleportToPosition();
	static void PlayerMagnet();
	
	static float GetGhostModeElapsedTime();
	static void Release();
};