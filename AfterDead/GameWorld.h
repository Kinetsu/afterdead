#pragma once

#include <Windows.h>

#include "GameObject.h"

namespace GameClasses {
	class GameWorld {
	public:
		char _0x0000[8];			//0x0000
		int FrameCounter;			//0x0008
		char _0x000C[32];			//0x000C
		DWORD MaxObjects;			//0x002C
		GameObject* FirstObject;	//0x0030
		int NumObjects;				//0x0034
		GameObject* LastObject;		//0x0038
		int Inited;					//0x003C
		int CurObjID;				//0x0040
		DWORD *ObjectList;			//0x0044

		GameObject* GetObjectById(int id) {
			return (GameObject*)(ObjectList[id]);
		}
	};
}