#pragma once

class HackConfig {
public:
	static const int PLAYER_MAX_DISTANCE = 300;
	static const int ZOMBIE_MAX_DISTANCE = 50;
	static const int OBJECT_MAX_DISTANCE = 300;
};