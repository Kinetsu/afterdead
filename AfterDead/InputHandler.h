#pragma once

#include "Core.h"
#include "KeyCode.h"
#include "HackEngine.h"
#include "GameFunctions.h"
#include "MenuUtil.h"

static bool x = false;
static void teste() {
	while (KeyCode::IsKeyHolding(VK_MENU)) {
		Core::LocalPlayer->SpeedGravity = -1.5;
		Core::LocalPlayer->PickupTime = 1.0f;
	}

	x = false;
}

class InputHandler {
public:
	static void HandleInput() {
		if (x == false && KeyCode::IsKeyPressed(VK_MENU)) {
			x = true;
			SetThreadPriority(CreateThread(NULL, NULL, (LPTHREAD_START_ROUTINE)teste, NULL, NULL, NULL), THREAD_PRIORITY_HIGHEST);
		}

		if (Core::LocalPlayer->Dead == 1 && KeyCode::IsKeyHolding(VK_MENU)) {
			if (KeyCode::IsKeyHolding(KeyCode::KeyW)) {
				Core::LocalPlayer->DeadPosition.z += 1;
			}

			if (KeyCode::IsKeyHolding(KeyCode::KeyS)) {
				Core::LocalPlayer->DeadPosition.z -= 1;
			}

			if (KeyCode::IsKeyHolding(KeyCode::KeyD)) {
				Core::LocalPlayer->DeadPosition.x += 1;
			}

			if (KeyCode::IsKeyHolding(KeyCode::KeyA)) {
				Core::LocalPlayer->DeadPosition.x -= 1;
			}

			if (KeyCode::IsKeyHolding(VK_UP)) {
				Core::LocalPlayer->DeadPosition.y += 0.5f;
			}

			if (KeyCode::IsKeyHolding(VK_DOWN)) {
				Core::LocalPlayer->DeadPosition.y -= 0.5f;
			}
		}

		if (Core::Menu->MenuItems[Menu::NoClip].Value.BoolValue) {
			if (KeyCode::IsKeyHolding(KeyCode::KeyV)) {
				HackEngine::NoClip();
			}

			if (KeyCode::IsKeyHolding(VK_UP)) {
				HackEngine::ElevatorUp();
			}

			if (KeyCode::IsKeyHolding(VK_DOWN)) {
				HackEngine::ElevatorDown();
			}

			if (KeyCode::IsKeyHolding(VK_LEFT)) {
				Core::LocalPlayer->Yaw += 2;
			}

			if (KeyCode::IsKeyHolding(VK_RIGHT)) {
				Core::LocalPlayer->Yaw -= 2;
			}

			if (KeyCode::IsKeyPressed(VK_ADD)) {
				HackEngine::NoClipSpeed++;
			}

			if (HackEngine::NoClipSpeed > 1 && KeyCode::IsKeyPressed(VK_SUBTRACT)) {
				HackEngine::NoClipSpeed--;
			}
		}

		if (Core::Menu->MenuItems[Menu::SpeedHack].Value.BoolValue) {
			if (KeyCode::IsKeyHolding(VK_SHIFT)) {
				HackEngine::NoClip(HackEngine::SpeedHackSpeed);
			}

			if (KeyCode::IsKeyPressed(VK_HOME)) {
				HackEngine::SpeedHackSpeed++;
			}

			if (HackEngine::SpeedHackSpeed > 1 && KeyCode::IsKeyPressed(VK_END)) {
				HackEngine::SpeedHackSpeed--;
			}
		}

		if (Core::Menu->MenuItems[Menu::GhostMode].Value.BoolValue && KeyCode::IsKeyPressed(VK_F2) && !KeyCode::IsKeyHolding(VK_MENU)) {
			HackEngine::GhostModeSwitch();
		}

		if (KeyCode::IsKeyPressed(VK_DIVIDE)) {
			Core::Menu->MenuItems[Menu::GhostJump].Value.BoolValue = !Core::Menu->MenuItems[Menu::GhostJump].Value.BoolValue;
		}

		if (Core::Menu->MenuItems[Menu::SuperJump].Value.BoolValue && KeyCode::IsKeyHolding(KeyCode::KeyQ)) {
			if (Core::Menu->MenuItems[Menu::GhostJump].Value.BoolValue && !HackEngine::LagSwitcher->IsOn) {
				HackEngine::IsGhostJumping = true;
				HackEngine::GhostModeSwitch();
			}

			if (!Core::Menu->MenuItems[Menu::GhostJump].Value.BoolValue
				|| (Core::Menu->MenuItems[Menu::GhostJump].Value.BoolValue && HackEngine::LagSwitcher->IsOn && HackEngine::GetGhostModeElapsedTime() > 0.5f)) {

				HackEngine::SuperJump();
			}
		} else if (!KeyCode::IsKeyHolding(KeyCode::KeyQ) && HackEngine::IsGhostJumping && HackEngine::LagSwitcher->IsOn) {
			HackEngine::IsGhostJumping = false;
			HackEngine::GhostModeSwitch();
		}

		if (Core::Menu->MenuItems[Menu::Aimbot].Value.BoolValue && KeyCode::IsKeyHolding(VK_RBUTTON)) {
			HackEngine::Aimbot();
		}

		if (Core::Menu->MenuItems[Menu::SuperBullet].Value.BoolValue) {
			HackEngine::SuperGun();
		}

		if (Core::Menu->MenuItems[Menu::PlaceObjectsEverywere].Value.BoolValue) {
			Core::LocalPlayer->IsValidPlace = 1;
		}

		if (Core::Menu->MenuItems[Menu::InstantDisconnect].Value.BoolValue && KeyCode::IsKeyPressed(VK_F1) && !KeyCode::IsKeyHolding(VK_MENU)) {
			HackEngine::Disconnect();
		}

		if (Core::Menu->MenuItems[Menu::InfiniteStamina].Value.BoolValue) {
			Core::LocalPlayer->Stamina = 120;
		}

		if (KeyCode::IsKeyPressed(VK_F3) && !KeyCode::IsKeyHolding(VK_MENU)) {
			Core::Menu->MenuItems[Menu::AutoHeal].Value.BoolValue = !Core::Menu->MenuItems[Menu::AutoHeal].Value.BoolValue;
		}

		if (Core::Menu->MenuItems[Menu::AutoHeal].Value.BoolValue) {
			HackEngine::AutoHeal();
		}

		if (Core::Menu->MenuItems[Menu::TeleportToPlayer].Value.BoolValue && KeyCode::IsKeyHolding(KeyCode::KeyH)) {
			HackEngine::TeleportToTargetPosition();
		}

		if (Core::Menu->MenuItems[Menu::RespawnOnDeadPosition].Value.BoolValue && Core::LocalPlayer->Dead == 1) {
			HackEngine::TeleportToDeadPosition();
		}

		if (Core::Menu->MenuItems[Menu::GoToSafePosition].Value.BoolValue && KeyCode::IsKeyPressed(VK_F5)) {
			HackEngine::TeleportToSafePosition();
		}

		if (KeyCode::IsKeyHolding(VK_MENU) && KeyCode::IsKeyHolding(KeyCode::KeyT)) {
			HackEngine::TeleportToPosition();
		}

		if (KeyCode::IsKeyPressed(VK_PRIOR)) {
			if (HackEngine::TeleportLocationIndex == 0) {
				HackEngine::TeleportLocationIndex = 9;
			} else {
				HackEngine::TeleportLocationIndex--;
			}
		}

		if (KeyCode::IsKeyPressed(VK_NEXT)) {
			if (HackEngine::TeleportLocationIndex == 9) {
				HackEngine::TeleportLocationIndex = 0;
			} else {
				HackEngine::TeleportLocationIndex++;
			}
		}

		if (KeyCode::IsKeyPressed(VK_MULTIPLY)) {
			Core::Menu->MenuItems[Menu::PlayerAutoMagnet].Value.BoolValue = !Core::Menu->MenuItems[Menu::PlayerAutoMagnet].Value.BoolValue;
		}

		if ((Core::Menu->MenuItems[Menu::PlayerMagnet].Value.BoolValue && KeyCode::IsKeyHolding(KeyCode::KeyG))
			|| (Core::Menu->MenuItems[Menu::PlayerMagnet].Value.BoolValue && Core::Menu->MenuItems[Menu::PlayerAutoMagnet].Value.BoolValue)) {
			HackEngine::PlayerMagnet();
		}

		if (KeyCode::IsKeyPressed(VK_MBUTTON)) {
			if (GameFunctions::IsValidPlayer(Core::Target)) {
				HackEngine::IsTargetLocked = !HackEngine::IsTargetLocked;
			}
		}
	}
};