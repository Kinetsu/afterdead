#pragma once
#pragma comment(lib, "Log.lib")
#pragma comment(lib, "Hook.lib")
#pragma comment(lib, "Draw.lib")
#pragma comment(lib, "Menu.lib")
#pragma comment(lib, "Radar.lib")

#include "GameRenderer.h"
#include "GameLogic.h"
#include "GameWorld.h"

#include <Log.h>
#include <Hook.h>
#include <Draw.h>
#include <Menu.h>
#include <Radar.h>

class Core {
public:
	static bool IsRuning;

	static HL::Log* Log;
	static HL::Hook* Hook;
	static HL::Draw* Draw;
	static HL::Menu* Menu;
	static HL::Menu* HUD;
	static HL::Radar* Radar;

	static GameClasses::GameRenderer* GameRenderer;
	static GameClasses::GameLogic* GameLogic;
	static GameClasses::GameWorld* GameWorld;
	static GameClasses::Player* LocalPlayer;
	static GameClasses::Player* Target;

	static void Init();
	static void InitDevice(LPDIRECT3DDEVICE9 device);
	static void Release();

	static char* GetCurrentPath();
	static char* ReadConfig(char* section, char* key, const char* defaultValue);
	static void ReadConfig(char* section, char* key, const char* defaultValue, char* output);
	static void WriteConfig(char* section, char* key, char* value);
};