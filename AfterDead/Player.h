#pragma once

#include "GameObject.h"
#include "Skeleton.h"

namespace GameClasses {
	class Player {
	public:
		virtual void v1();
		virtual void v2();
		virtual void v3();
		virtual void v4();
		virtual void v5();
		virtual void v6();
		virtual void v7();
		virtual void v8();
		virtual void v9();
		virtual void v10();
		virtual void v11();
		virtual void v12();
		virtual void v13();
		virtual void v14();
		virtual void v15();
		virtual void v16();
		virtual void v17();
		virtual void SetPosition(const D3DXVECTOR3& pos);

		char pad_0x0004[0x8]; //0x0004
		D3DXVECTOR3 SomePosition1; //0x000C 
		char pad_0x0018[0xC]; //0x0018
		D3DXVECTOR3 Position; //0x0024 
		float GameBodyYaw; //0x0030 
		char pad_0x0034[0x8]; //0x0034
		float GameBodyYaw2; //0x003C 
		char pad_0x0040[0x8]; //0x0040
		int NetworkId; //0x0048 
		char pad_0x004C[0x144]; //0x004C
		D3DXVECTOR3 SomePositionDead; //0x0190 
		char pad_0x019C[0xC]; //0x019C
		D3DXVECTOR3 SomePosition2; //0x01A8 
		char pad_0x01B4[0x54]; //0x01B4
		D3DXVECTOR3 SomePosition3; //0x0208 
		char pad_0x0214[0x58]; //0x0214
		D3DXVECTOR3 SomePosition4; //0x026C 
		float NothSouthSpeed; //0x0278 
		char pad_0x027C[0x4]; //0x027C
		float EastWestSpeed; //0x0280 
		float GameBodyYaw3; //0x0284 
		char pad_0x0288[0x8]; //0x0288
		float SomeCounter; //0x0290 
		char pad_0x0294[0xF4]; //0x0294
		int IsInSafeZone; //0x0388 
		float TimeOfRespawn; //0x038C 
		char pad_0x0390[0x4A0]; //0x0390
		float SomeCounterWhileDead; //0x0830 
		int DeadTimerCountdown; //0x0834 
		char pad_0x0838[0x2C]; //0x0838
		int GroupId; //0x0864 
		DWORD NameXOR; //0x0868 
		char pad_0x086C[0x3C]; //0x086C
		int NameLength; //0x08A8 
		char pad_0x08AC[0x9C]; //0x08AC
		int BackpackId; //0x0948 
		char pad_0x094C[0x38]; //0x094C
		float Health; //0x0984 
		float Hunger; //0x0988 
		float Thirst; //0x098C 
		float Toxic; //0x0990 
		char pad_0x0994[0x18]; //0x0994
		D3DXVECTOR3 SpawnPosition; //0x09AC 
		char pad_0x09B8[0x4]; //0x09B8
		int IsInSafeZone2; //0x09BC 
		char pad_0x09C0[0x18]; //0x09C0
		int Reputation; //0x09D8 
		char pad_0x09DC[0xC]; //0x09DC
		int TotalKills; //0x09E8 
		int TotalDeaths; //0x09EC 
		char pad_0x09F0[0x108]; //0x09F0
		InventoryItem WeaponsSlot[2]; //0x0AF8 
		InventoryItem QuickSlots[6]; //0x0BC8 
		InventoryItem UnknownItem; //0x0E38 
		InventoryItem ArmorSlots[2]; //0x0EA0 
		InventoryItem BagSlots[125]; //0x0F70 
		char pad_0x4238[0x1A0]; //0x4238
		unsigned char Unknown2; //0x43D8 
		unsigned char AggressorFlag; //0x43D9 
		char pad_0x43DA[0x6]; //0x43DA
		float AggressorTimer; //0x43E0 
		char pad_0x43E4[0x20C]; //0x43E4
		unsigned char Unknown; //0x45F0 
		unsigned char IsValidPlace; //0x45F1 
		char pad_0x45F2[0xE]; //0x45F2
		Skeleton* Skeleton; //0x4600 
		char pad_0x4604[0x8]; //0x4604
		D3DXVECTOR3 LastTargetHitLocation; //0x460C 
		D3DXVECTOR3 LastTargetHitLocationNorm; //0x4618 
		char pad_0x4624[0x8]; //0x4624
		int PlayerState; //0x462C 
		int PlayerMoveDir; //0x4630 
		int PlayerState2; //0x4634 
		char pad_0x4638[0xC]; //0x4638
		Weapon* WeaponPrimary; //0x4644 
		Weapon* WeaponSecondary; //0x4648 
		char pad_0x464C[0x10]; //0x464C
		Weapon* Fists; //0x465C 
		int SelectedWeapon; //0x4660 
		int PreviousSelectedWeapon; //0x4664 
		unsigned char IsPressedFireTrigger; //0x4668 
		unsigned char IsAiming; //0x4669 
		unsigned char IsChangeAiming; //0x466A 
		unsigned char IsInScope; //0x466B 
		char pad_0x466C[0x10]; //0x466C
		float CounterByTheLastRespawn; //0x467C 
		char pad_0x4680[0x8]; //0x4680
		float PickupTime; //0x4688 
		char pad_0x468C[0x1C]; //0x468C
		float PitchOnDead; //0x46A8 
		float PitchOnDead2; //0x46AC 
		char pad_0x46B0[0x84]; //0x46B0
		D3DXVECTOR3 PlayerLocalVelocity; //0x4734 
		float PlayerLocalMovingSpeed; //0x4740 
		float CameraHeight; //0x4744 
		float Stamina; //0x4748 
		char pad_0x474C[0x1C]; //0x474C
		float LastTimeHit; //0x4768 
		char pad_0x476C[0x4]; //0x476C
		D3DXVECTOR3 LastTimeHitForce; //0x4770 
		char pad_0x477C[0x4]; //0x477C
		int Dead; //0x4780 
		float TimeOfDeath; //0x4784 
		int DeathDamageSource; //0x4788 
		D3DXVECTOR3 DeadPosition; //0x478C 
		float TimeOfLastRespawn; //0x4798 
		int KillerId; //0x479C 
		unsigned char IsKillerView; //0x47A0 
		unsigned char IsCrouched; //0x47A1 
		unsigned char IsProned; //0x47A2 
		unsigned char IsOnGround; //0x47A3 
		float HeightAboveGround; //0x47A4 
		float JumpVelocity; //0x47A8 
		char pad_0x47AC[0x14]; //0x47AC
		float GameCounter; //0x47C0 
		char pad_0x47C4[0x64]; //0x47C4
		float Yaw; //0x4828 
		float Pitch; //0x482C 
		char pad_0x4830[0x4]; //0x4830
		float SpeedLeftRight1; //0x4834 
		float SpeedGravity; //0x4838 
		float SpeedFrontBack1; //0x483C 
		float SpeedLeftRight2; //0x4840 
		char pad_0x4844[0x4]; //0x4844
		float SpeedFrontBack2; //0x4848 
		char pad_0x484C[0x50]; //0x484C
		D3DXMATRIX BoneCoeff; //0x489C 
		D3DXMATRIX DrawFullMatrix; //0x48DC 
		D3DXMATRIX DrawFullMatrix_Localized; //0x491C 
		D3DXMATRIX MoveMatrix; //0x495C 
		char pad_0x499C[0x18]; //0x499C
		float LastTimeWeaponSwitched; //0x49B4 
		D3DXVECTOR3 ViewTargetPosition; //0x49B8 
		D3DXVECTOR3 ViewTargetNorm; //0x49C4 
		char pad_0x49D0[0x24]; //0x49D0
		float MovingQuantity; //0x49F4 
		char pad_0x49F8[0x8]; //0x49F8
		float MovingTimer; //0x4A00 

		char* GetNameXor(void) {
			return (char*)((DWORD)(this) + 0x0868);
		}

		void GetName(PCHAR outName) {
			if (this->NameLength < 1 || this->NameLength > 60) {
				outName = "";
				return;
			}

			memcpy(outName, GetNameXor(), this->NameLength + 1);
			for (int i = 0; i < this->NameLength; i++) {
				outName[i] ^= i - 87;
			}
		}

		D3DXVECTOR3 GetBonePosition(int boneID) {
			D3DXVECTOR3 position = D3DXVECTOR3(0, 0, 0);

			if (!Skeleton || !Skeleton->BoneInfo)
				return position;

			D3DXMATRIX rotationMatrix;
			D3DXMATRIX boneMatrix;
			D3DXMATRIX resultMatrix;

			D3DXMatrixRotationYawPitchRoll(&rotationMatrix, 0.0, D3DXToRadian(90), 0.0);
			boneMatrix = Skeleton->BoneInfo->Bones[boneID].BoneMatrix;
			D3DXMatrixMultiply(&boneMatrix, &boneMatrix, &BoneCoeff);
			D3DXMatrixMultiply(&resultMatrix, &rotationMatrix, &boneMatrix);

			position.x = resultMatrix._41;
			position.y = resultMatrix._42;
			position.z = resultMatrix._43;

			return position;
		}

		D3DXVECTOR3 GetHeadPosition() {
			D3DXVECTOR3 headPosition = this->GetBonePosition(10);
			headPosition.y += 0.1f;

			return headPosition;
		}

		float GetHeath() {
			if (this->Health == 0 && this->Dead == 0) {
				return 100.0f;
			}

			return this->Health;
		}
	};
}