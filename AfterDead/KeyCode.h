#pragma once

class KeyCode {
public:
		static const int Key0 = 0x30; //0 key
		static const int Key1 = 0x31; //1 key
		static const int Key2 = 0x32; //2 key
		static const int Key3 = 0x33; //3 key
		static const int Key4 = 0x34; //4 key
		static const int Key5 = 0x35; //5 key
		static const int Key6 = 0x36; //6 key
		static const int Key7 = 0x37; //7 key
		static const int Key8 = 0x38; //8 key
		static const int Key9 = 0x39; //9 key
		static const int KeyA = 0x41; //A key
		static const int KeyB = 0x42; //B key
		static const int KeyC = 0x43; //C key
		static const int KeyD = 0x44; //D key
		static const int KeyE = 0x45; //E key
		static const int KeyF = 0x46; //F key
		static const int KeyG = 0x47; //G key
		static const int KeyH = 0x48; //H key
		static const int KeyI = 0x49; //I key
		static const int KeyJ = 0x4A; //J key
		static const int KeyK = 0x4B; //K key
		static const int KeyL = 0x4C; //L key
		static const int KeyM = 0x4D; //M key
		static const int KeyN = 0x4E; //N key
		static const int KeyO = 0x4F; //O key
		static const int KeyP = 0x50; //P key
		static const int KeyQ = 0x51; //Q key
		static const int KeyR = 0x52; //R key
		static const int KeyS = 0x53; //S key
		static const int KeyT = 0x54; //T key
		static const int KeyU = 0x55; //U key
		static const int KeyV = 0x56; //V key
		static const int KeyW = 0x57; //W key
		static const int KeyX = 0x58; //X key
		static const int KeyY = 0x59; //Y key
		static const int KeyZ = 0x5A; //Z key

		static bool IsKeyPressed(int keyCode) {
			return GetAsyncKeyState(keyCode) & 0x1;
		}

		static bool IsKeyHolding(int keyCode) {
			return GetAsyncKeyState(keyCode) & 0x8000;
		}
};