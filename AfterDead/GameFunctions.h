#pragma once

#include "GameObject.h"
#include "Player.h"

class GameFunctions {
public:
	typedef GameClasses::BaseItemConfig* (__stdcall* tGetItemConfig)(DWORD itemID);
	
	static tGetItemConfig GetItemConfig;
	static bool WorldToScreen(const D3DXVECTOR3& worldPosition, D3DXVECTOR3* screenPosition);
	static D3DXVECTOR2 GetScreenCenter();
	static bool IsValidPlayer(GameClasses::Player* player);
	static bool IsConnected();
	static void SetConnected(bool connected);
};
