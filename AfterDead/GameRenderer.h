#pragma once
#pragma comment(lib, "d3d9.lib")
#pragma comment(lib, "d3dx9.lib")

#include <Windows.h>
#include <d3d9.h>
#include <d3dx9.h>

namespace GameClasses {
	class GameRenderer {
	public:
		struct WindownsSize {
			DWORD Width;
			DWORD Height;
		};
		virtual void unk00() = 0;
		virtual void unk01() = 0;
		virtual void unk02() = 0;
		virtual void unk03() = 0;
		virtual void* GetDevice();
		virtual void unk05() = 0;
		virtual WindownsSize* GetSize();
	};
}