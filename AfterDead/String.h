#pragma once

#include <string>

class String {
public:
	static std::string ReplaceAll(std::string text, const std::string& from, const std::string& to) {
		size_t start_pos = 0;

		while ((start_pos = text.find(from, start_pos)) != std::string::npos) {
			text.replace(start_pos, from.length(), to);
			start_pos += to.length();
		}
		return text;
	}
};