#pragma comment(lib, "Memory.lib")

#include "Core.h"
#include "HookedFunctions.h"
#include "GameFunctions.h"
#include "HackEngine.h"
#include <Memory.h>

//#pragma comment(lib, "detours.lib")
//#include <detours.h>

#pragma region Atributos
HL::Log* Core::Log = new HL::Log(false, false);
HL::Hook* Core::Hook = new HL::Hook();
HL::Draw* Core::Draw = NULL;
HL::Menu* Core::Menu = NULL;
HL::Menu* Core::HUD = NULL;
HL::Radar* Core::Radar = NULL;

GameClasses::GameRenderer* Core::GameRenderer = NULL;
GameClasses::GameLogic* Core::GameLogic = NULL;
GameClasses::GameWorld* Core::GameWorld = NULL;
GameClasses::Player* Core::LocalPlayer = NULL;
GameClasses::Player* Core::Target = NULL;

bool Core::IsRuning = true;
#pragma endregion

void Core::Init() {
	Core::Log->Write("Initializing");

	#pragma region Getting GameRender
	DWORD dwRenderAddress = HL::Memory::FindPattern((BYTE*)"\x85\xC0\x74\x26\x8B\x0D", "xxxxxx");
	dwRenderAddress = *(DWORD*)*(DWORD*)(dwRenderAddress + 6);

	Core::Log->Write("GameRenderer 0x%p", dwRenderAddress);
	Core::GameRenderer = (GameClasses::GameRenderer*) dwRenderAddress;
	#pragma endregion

	#pragma region Getting GameLogic
	DWORD dwGameLogic = HL::Memory::FindPattern((BYTE*)"\x55\x8B\xEC\x83\xEC\x0C\x83\x3D\x00\x00\x00\x00\x00\x74\x38", "xxxxxxxx?????xx");
	dwGameLogic = *(DWORD*)*(DWORD*)(dwGameLogic + 8);

	Core::Log->Write("GameLogic: 0x%p", dwGameLogic);
	Core::GameLogic = (GameClasses::GameLogic*) dwGameLogic;
	#pragma endregion

	#pragma region Getting GameWorld
	DWORD dwGameWorld = HL::Memory::FindPattern((BYTE*)"\x55\x8B\xEC\x6A\xFF\x68\x00\x00\x00\x00\x64\xA1\x00\x00\x00\x00\x50\x64\x89\x25\x00\x00\x00\x00\x83\xEC\x0C\x83\x3D\x00\x00\x00\x00\x00\x74\x17", "xxxxxx????xx????xxxx????xxxxx?????xx");
	dwGameWorld = *(DWORD*)*(DWORD*)(dwGameWorld + 29);

	Core::Log->Write("GameWorld: 0x%p", dwGameWorld);
	Core::GameWorld = (GameClasses::GameWorld*) dwGameWorld;
	#pragma endregion

	#pragma region Getting ItemConfig
	DWORD dwGetItemConfig = HL::Memory::FindPattern((BYTE*)"\x8B\x0D\x00\x00\x00\x00\x8B\x54\x24\x04", "xx????xxxx");

	Core::Log->Write("GetItemConfig: 0x%p", dwGetItemConfig);
	GameFunctions::GetItemConfig = (GameFunctions::tGetItemConfig)(dwGetItemConfig);
	#pragma endregion

	/*#pragma region SendToHost
	DWORD dwSendToHost = HL::Memory::FindPattern((BYTE*)"\xE8\x00\x00\x00\x00\x83\xFB\x01\x7E\x17", "x????xxxxx");

	Core::Log->Write("SendToHost: 0x%p", dwSendToHost);
	GameFunctions::OriginalSendToHost = (GameFunctions::tSendToHost)(dwSendToHost);

	DetourTransactionBegin();
	DetourUpdateThread(GetCurrentThread());

	DetourAttach(&(PVOID&)GameFunctions::OriginalSendToHost, GameFunctions::Teste);
	DetourTransactionCommit();
	#pragma endregion*/

	#pragma region Hooking
	Core::Hook->HookPresent(HookedFunctions::HookedPresent);
	#pragma endregion
}

void Core::InitDevice(LPDIRECT3DDEVICE9 device) {
	#pragma region Inicialização dos objetos
	Core::Draw = new HL::Draw(device);
	Core::Radar = new HL::Radar(Core::Draw);
	Core::Menu = new HL::Menu(Core::Draw, HL::Color::DARK_GRAY, HL::Color::BLACK);
	Core::HUD = new HL::Menu(Core::Draw, HL::Draw::ToAlphaColor(HL::Color::DARK_GRAY, 100), HL::Color::BLACK);
	#pragma endregion

	#pragma region Inicialização das fontes
	Core::Draw->AddFont("Tahoma Small", 16, strcmp(Core::ReadConfig("Configs", "UseBoldFonts", "FALSE"), "TRUE") == 0 ? FW_SEMIBOLD : FW_NORMAL, "Tahoma");
	Core::Draw->AddFont("Tahoma Medium", 24, strcmp(Core::ReadConfig("Configs", "UseBoldFonts", "FALSE"), "TRUE") == 0 ? FW_SEMIBOLD : FW_NORMAL, "Tahoma");
	Core::Draw->SetFont("Tahoma Small");
	#pragma endregion

	#pragma region Inicialização do Menu e Radar
	HL::ItemValue value;
	value.BoolValue = true;

	HL::ItemValue valueAux;
	valueAux.BoolValue = false;

	Core::Menu->AddMenuItem("ESP", HL::ItemType::Label, HL::Color::CYAN, HL::Color::CYAN);
	Core::Menu->AddMenuItem("Player Skeleton", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Player Info", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Optimized Player Info", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Zombie Skeleton", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Zombie Info", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Item Info", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);

	Core::Menu->AddMenuItem("CHEATING", HL::ItemType::Label, HL::Color::CYAN, HL::Color::CYAN);
	Core::Menu->AddMenuItem("Aimbot [RM_BUTTOM]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Instant Disconnect [F1]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Ghost Mode [F2]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Auto Heal [F3]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Go To SafePosition [F5]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Super Jump [Q]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Ghost Jump [/]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, valueAux);
	Core::Menu->AddMenuItem("NoClip [V]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Player Magnet [G]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Player Auto Magnet [*]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, valueAux);
	Core::Menu->AddMenuItem("Speed Hack [SHIFT]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Teleport To Player [H]", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Super Bullet", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Infinite Stamina", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Respawn on Dead Position", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Place Objects Everywere", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, value);
	Core::Menu->AddMenuItem("Keep Target Locked on Death", HL::ItemType::CheckBox, HL::Color::WHITE, HL::Color::LIME, HL::ItemValueType::BoolValue, valueAux);

	Core::Menu->X = 15;
	Core::Menu->Y = 450;
	Core::Menu->Width = 250;

	Core::Radar->X = Core::GameRenderer->GetSize()->Width - Core::Radar->RadarSize * 0.75;
	Core::Radar->Y = Core::Radar->RadarSize * 0.75;
	#pragma endregion

	#pragma region Inicialização do HUD
	Core::HUD->AddMenuItem("Server:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Ping:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Kills:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Deaths:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Reputation:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Health:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Backpack:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("1st Weapon:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("2nd Weapon:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Noclip Speed:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Speedhack Speed:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Ghost Jump:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Auto Magnet:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Auto Heal:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Compass:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Teleport:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);
	Core::HUD->AddMenuItem("Aimbot Target:", HL::ItemType::Label, HL::Color::WHITE, HL::Color::WHITE, HL::ItemValueType::StringValue, value);

	Core::HUD->X = 15;
	Core::HUD->Y = 50;
	Core::HUD->Width = 250;
	Core::HUD->Cursor = false;
	Core::HUD->ShowAways = true;
	Core::HUD->UseDifferentColorForValues = true;
	Core::HUD->ValueColor = HL::Color::LIME;
	#pragma endregion
}

void Core::Release() {
	Core::Log->Write("Releasing Hook");
	Core::Hook->ReleasePresent();

	Core::Log->Write("Releasing DrawEngine");
	Core::Draw->Release();

	Core::Log->Write("Releasing HackEngine");
	HackEngine::Release();

	Core::Log->Write("Releasing Log");
	Core::Log->Release();
}

char* Core::ReadConfig(char* section, char* key, const char* defaultValue) {
	char result[255];
	memset(result, 0x00, 255);
	GetPrivateProfileString(section, key, defaultValue, result, 255, ".\\Config.ini");
	return result;
}

void Core::ReadConfig(char* section, char* key, const char* defaultValue, char* output) {
	GetPrivateProfileString(section, key, defaultValue, output, 255, ".\\Config.ini");
}

void Core::WriteConfig(char* section, char* key, char* value) {
	WritePrivateProfileString(section, key, value, ".\\Config.ini");
}

char* Core::GetCurrentPath() {
	char* result;

	_get_pgmptr(&result);

	return result;
}