#pragma once

#include "Player.h"
#include "Offsets.h"

namespace GameClasses {
	class GameLogic {
	public:
		char _0x0000[16];	//0x0000
		int Ping;			//0x0010 
		char _0x0014[4];	//0x0014
		DWORD Players[80];	//0x0018 

		Player* GetLocalPlayer() {
			return (Player*)(*(DWORD*)((DWORD)(this) + Offsets::LocalPlayer));
		}

		Player* GetPlayerByIndex(int index) {
			return (Player*)(*(DWORD*)((DWORD)(this) + 0x18 + (index * 4)));
		}

		char* GetServerName() {
			return (char*)(((DWORD)(this)) + Offsets::ServerName);
		}
	};
}