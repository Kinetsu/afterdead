﻿#pragma once

namespace GameObjectEnums {
	enum PlayerState {
		Idle				= 0,
		IdleAim				= 1,
		Crouch				= 2,
		CrouchAim			= 3,
		WalkingAim			= 4,
		Walking				= 6,
		Runing				= 7,
		ProneAim			= 9,
		Lifting				= 11,
		Proning				= 11,
		Prone				= 12,
		Dead				= 19
	};

	enum ZombieState {
		zDead				= 0,
		zIdle				= 7,
		zChasing			= 8,
		zHitting			= 10
	};

	enum ObjectType {
		Human				= 0x21,
		Zombie				= 0x20001,
		Trap				= 0x40001,
		DroppedItem			= 0x240005,
		Barricade			= 0x2040000
	};

	enum StoreCategories {
		Invalid				= 0,
		Account				= 1,
		Boost				= 2,
		LootBox				= 7,

		Melnar				= 10,
		Armor				= 11,
		Backpack			= 12,
		Helmet				= 13,
		HeroPackage			= 16,

		FPSAttachment		= 19,

		ASR					= 20,
		SNP					= 21,
		SHTG				= 22,
		MG					= 23,
		HG					= 25,
		SMG					= 26,
		Grenade				= 27,
		UsableItem			= 28,
		Melee				= 29,
		Food				= 30,
		Water				= 33,
		Traps				= 34,
		Medicines			= 35,
		ItemSkin			= 37,
		ItemBox				= 38,
		Bullets				= 40,
		DisassembleItems	= 44,

		Components			= 50,
		CraftRecipe			= 51,
		
		Fuel				= 54,
		Vehicle				= 55,
		Case				= 58
	};
}